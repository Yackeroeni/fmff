{-#LANGUAGE GADTs, RankNTypes, TypeOperators, UndecidableInstances,
 FlexibleInstances, FunctionalDependencies, FlexibleContexts, AllowAmbiguousTypes,
 DeriveFunctor #-}

module WhatMorphism.Free where

import Control.Applicative

-- Setup

data Free f a where
    Var :: a -> Free f a
    Con :: f (Free f a) -> Free f a

foldFree :: Functor f => (a -> b) -> (f b -> b) -> Free f a -> b
foldFree gen alg (Var x) = gen x
foldFree gen alg (Con op) = alg (fmap (foldFree gen alg) op)

instance Functor f => Functor (Free f) where
    fmap f (Var x) = Var (f x)
    fmap f (Con op) = Con (fmap (fmap f) op)

instance Functor f => Applicative (Free f) where
    pure = Var
    Var f <*> as = fmap f as
    Con faf <*> as = Con (fmap (<*> as) faf)

instance Functor f => Monad (Free f) where
    return = Var
    m >>= f = foldFree f Con m

data (+) f g a where
    Inl :: f a -> (f + g) a
    Inr :: g a -> (f + g) a

instance (Functor f, Functor g) => Functor (f + g) where
    fmap f (Inl s) = Inl (fmap f s)
    fmap f (Inr s) = Inr (fmap f s)

junction :: (f b -> b) -> (g b -> b) -> ((f + g) b -> b)
junction algF algG (Inl s) = algF s
junction algF algG (Inr s) = algG s

instance (Show a, Show (f (Free f a))) => Show (Free f a) where
    show (Var x) = "Var " ++ show x
    show (Con op) = "Con (" ++ show op ++ ")"

instance (Show (f a), Show (g a)) => Show ((f + g) a) where
    show (Inl x) = "Inl (" ++ show x ++ ")"
    show (Inr x) = "Inr (" ++ show x ++ ")"

class Functor f => TermAlgebra h f | h -> f where
    var :: forall a . a -> h a
    con :: forall a . f (h a) -> h a

instance Functor f => TermAlgebra (Free f) f where
    var = Var
    con = Con

class (Monad m, TermAlgebra m f) => TermMonad m f | m -> f
instance (Monad m, TermAlgebra m f) => TermMonad m f

newtype Codensity h a = Codensity {unCod :: forall x . (a -> h x) -> h x}
    deriving (Functor)

instance Applicative (Codensity h) where
    pure = return
    mf <*> mx = do
        f <- mf
        x <- mx
        return (f x)

instance Monad (Codensity h) where
    return x = Codensity (\k -> k x)
    Codensity m >>= f = Codensity (\k -> m (\a -> unCod (f a) k)) 

runCod :: (a -> f x) -> Codensity f a -> f x
runCod g m = unCod m g

instance TermAlgebra h f => TermAlgebra (Codensity h) f where
    var = return
    con = algCod con

algCod :: Functor f => (forall x . f (h x) -> h x) -> f (Codensity h a) -> Codensity h a
algCod alg op = Codensity (\k -> alg (fmap (\m -> unCod m k) op))

instance TermAlgebra h f => TermMonad (Codensity h) f where

newtype Comp f g a = Comp { runComp :: f (g a) }
