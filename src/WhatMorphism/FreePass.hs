{-# LANGUAGE TemplateHaskell #-}
--------------------------------------------------------------------------------
module WhatMorphism.FreePass
    ( freePass
    , reworkFree
    ) where


--------------------------------------------------------------------------------
import           Control.Monad         
import           Control.Monad.Error   (catchError)
import           Control.Monad.Trans   (lift)
import           CoreSyn               (Bind (..), CoreBind, Expr (..))
import qualified CoreSyn               as CoreSyn
import           Data.Maybe            (isJust)
import           Data.Monoid           (Monoid (..))
import           DataCon               (DataCon)
import qualified DataCon               as DataCon
import qualified MkCore                as MkCore
import qualified Outputable            as Outputable
import           Type                  (Type)
import qualified Type                  as Type
import           Var                   (Var)
import qualified Var                   as Var
import           TyCon                 as TyCon
import           OccName               as OccName
import           Name                  as Name
import qualified UniqSupply            as Unique
import qualified Unique                as Unique
import qualified SrcLoc                as SrcLoc
import           Data.Map                 (Map)
import qualified Data.Map                 as M
import           Kind                  
import           CoreMonad             as CoreMonad
import Class
import IdInfo
import Data.List
import Data.Maybe
import TcRnMonad

import Control.Monad.Trans.State.Lazy

import Language.Haskell.TH hiding (Type)

import TcEnv
import InstEnv

import GhcPlugins

import          HERMIT.GHC.Typechecker

--------------------------------------------------------------------------------

import           WhatMorphism.Dump
import           WhatMorphism.Expr
import           WhatMorphism.RewriteM
import qualified WhatMorphism.Free as Free
import           WhatMorphism.Free hiding (Var)
import           WhatMorphism.FreeFusion
import           WhatMorphism.FreeUtils

--------------------------------------------------------------------------------

-- TODO check for stuctural recursion before changing to fold?

freePass :: [CoreBind] -> RewriteM [CoreBind]
freePass cbs = do 
                (cbs', fr) <- runStateT (mapM freePass' cbs) (FreeRegister { reworkedFrees = M.empty })
                runFusion fr cbs'
    where
        freePass' cb =  
            (case cb of
                NonRec f e -> reworkFree' f e >>= (\e' -> return $ NonRec f e')
                Rec bs -> liftM (Rec) $ mapM (\(f, e) -> reworkFree' f e >>= \e' -> return $ (f, e')) bs)
        reworkFree' f e = flip catchError (report f e) (reworkFree'' f e)
        report f e err = do
            lift $ message $ "====== Error: " ++ err
            return e

reworkFree'' :: Var -> Expr Var -> StateT FreeRegister RewriteM CoreExpr
reworkFree'' f e = do
        fi <- lift $ reworkFree f e
        s <- get
        put $ FreeRegister { reworkedFrees = M.insert f fi (reworkedFrees s) }
        return $ body fi
    

-- TODO remove duplication with stuff below
reworkFree :: Var -> Expr Var -> RewriteM FreeInfo
reworkFree f body = do
    mg <- rewriteModGuts <$> rewriteAsk

    liftCoreM $ Outputable.pprTrace "f " (Outputable.ppr f) $ return ()
    liftCoreM $ Outputable.pprTrace "body " (Outputable.ppr body) $ return ()

    Just freeName <- liftCoreM $ CoreMonad.thNameToGhcName ''Free.Free
    freeTyCon <- liftCoreM $ lookupTyCon freeName
    Just plusName <- liftCoreM $ CoreMonad.thNameToGhcName ''(+)
    plus <- liftCoreM $ lookupTyCon plusName

    -- collect some basic information about the function we're transforming
    let fTy = Var.varType f
        (forAlls, rest) = Type.splitForAllTys fTy
        (argTys , rTy) = Type.splitFunTys rest
        (fTyBinders, fValBinders, body') = CoreSyn.collectTyAndValBinders body
    (rTyCon, _rTyArgs) <- liftMaybe "Return Free has no TyCon" $
        Type.splitTyConApp_maybe rTy

    let bMap = binderMap' forAlls (takeWhile isClassPred argTys) (fTyBinders ++ fValBinders) 
    let dictLookup = findDictionary mg plus forAlls (takeWhile isClassPred argTys) (fTyBinders ++ fValBinders)

    unless (rTyCon == freeTyCon) $
        fail $ "return is not free " ++ dump rTyCon

    let realArgs = dropWhile isClassPred argTys

    -- TODO remove this thing below by some maybe stuff
    if null realArgs then
        reworkBase f body
        else let origInFree = head $ dropWhile isClassPred argTys
                 inFree = bMap $ origInFree
                 t = Type.splitTyConApp_maybe inFree
             in if not (isJust t) then 
                 reworkBase f body
                 else let (inTyCon, _inTyArgs) = fromJust t
                      in if (inTyCon /= freeTyCon) then 
                         reworkBase f body
                         else reworkFull f body


-- TODO remove the parts you dont need
reworkBase :: Var -> Expr Var -> RewriteM FreeInfo
reworkBase f body = do
    mg <- rewriteModGuts <$> rewriteAsk

    Just freeName <- liftCoreM $ CoreMonad.thNameToGhcName ''Free.Free
    freeTyCon <- liftCoreM $ lookupTyCon freeName
    Just freeVarName <- liftCoreM $ CoreMonad.thNameToGhcName 'Free.Var
    varDataCon <- liftCoreM $ lookupDataCon freeVarName
    Just freeConName <- liftCoreM $ CoreMonad.thNameToGhcName 'Free.Con
    conDataCon <- liftCoreM $ lookupDataCon freeConName
    Just foldFreeName <- liftCoreM $ CoreMonad.thNameToGhcName 'Free.foldFree
    foldF <- liftCoreM $ lookupId foldFreeName
    Just runCompName <- liftCoreM $ CoreMonad.thNameToGhcName 'Free.runComp
    runComp <- liftCoreM $ lookupId runCompName
    Just runCodName <- liftCoreM $ CoreMonad.thNameToGhcName 'Free.runCod
    runCod <- liftCoreM $ lookupId runCodName
    Just compName <- liftCoreM $ CoreMonad.thNameToGhcName 'Free.Comp
    comp <- liftCoreM $ lookupDataCon compName
    Just codName <- liftCoreM $ CoreMonad.thNameToGhcName 'Free.Codensity
    cod <- liftCoreM $ lookupDataCon codName
    Just plusName <- liftCoreM $ CoreMonad.thNameToGhcName ''(+)
    plus <- liftCoreM $ lookupTyCon plusName
    Just functorName <- liftCoreM $ CoreMonad.thNameToGhcName ''Functor
    fun <- liftCoreM $ lookupTyCon functorName
    Just fmapName <- liftCoreM $ CoreMonad.thNameToGhcName 'fmap
    fm <- liftCoreM $ lookupId fmapName
    Just applicativeName <- liftCoreM $ CoreMonad.thNameToGhcName ''Applicative
    app <- liftCoreM $ lookupTyCon applicativeName
    Just monadName <- liftCoreM $ CoreMonad.thNameToGhcName ''Monad
    mon <- liftCoreM $ lookupTyCon monadName
    Just returnName <- liftCoreM $ CoreMonad.thNameToGhcName 'return
    ret <- liftCoreM $ lookupId returnName
    Just bindName <- liftCoreM $ CoreMonad.thNameToGhcName '(>>=)
    bind <- liftCoreM $ lookupId bindName

    let Just fClass = tyConClass_maybe fun
        Just appClass = tyConClass_maybe app
        Just mClass = tyConClass_maybe mon

    let plusVars = tyConTyVars plus
        plusFunctorInner = (mkTyConApp plus (map mkTyVarTy (take (length plusVars - 1) plusVars)))
    instPlus <- liftCoreM $ findInstance mg fClass [plusFunctorInner]

    let fTy = Var.varType f
        (forAlls, rest) = Type.splitForAllTys fTy
        (argTys , rTy) = Type.splitFunTys rest
        (fTyBinders, fValBinders, body') = CoreSyn.collectTyAndValBinders body
    (rTyCon, _rTyArgs) <- liftMaybe "Return Free has no TyCon" $
        Type.splitTyConApp_maybe rTy


    unless (null argTys) $ fail "Not yet implemented"

    let bMap = binderMap' forAlls (takeWhile isClassPred argTys) (fTyBinders ++ fValBinders) 
    let dictLookup = findDictionary mg plus forAlls (takeWhile isClassPred argTys) (fTyBinders ++ fValBinders)

    unless (rTyCon == freeTyCon) $
        fail $ "return is not free " ++ dump rTyCon

    let realArgs = dropWhile isClassPred argTys
        origInFree = head $ dropWhile isClassPred argTys
        inFree = bMap $ origInFree
    let retFree         = [rTy]
        outA            = bMap $ head (tail (snd (splitTyConApp (last retFree))))
        origOutG        = head (snd (splitTyConApp (last retFree)))
        outG            = bMap $ origOutG

    let returnFree = bMap $ (\(fr, rs) -> mkTyConApp fr (take (length rs - 1) rs)) (splitTyConApp (last retFree))

    abstrVars <- mkAbstrVars returnFree outG
    body' <- replacevcb varDataCon conDataCon (varv abstrVars) (conv abstrVars) (bindv abstrVars) body

    liftCoreM $ Outputable.pprTrace "replaced" (Outputable.ppr body') $ return ()

    let abstractBody = mkCoreLams (sequence [varv, conv, bindv] abstrVars) body'

    liftCoreM $ Outputable.pprTrace "abstractBody" (Outputable.ppr abstractBody) $ return ()

    return $ EndInfo 
        { body = body
        , abstractBody = abstractBody
        , inA = outA}
        

reworkFull :: Var -> Expr Var -> RewriteM FreeInfo
reworkFull f body = do
    mg <- rewriteModGuts <$> rewriteAsk

    Just freeName <- liftCoreM $ CoreMonad.thNameToGhcName ''Free.Free
    freeTyCon <- liftCoreM $ lookupTyCon freeName
    Just freeVarName <- liftCoreM $ CoreMonad.thNameToGhcName 'Free.Var
    varDataCon <- liftCoreM $ lookupDataCon freeVarName
    Just freeConName <- liftCoreM $ CoreMonad.thNameToGhcName 'Free.Con
    conDataCon <- liftCoreM $ lookupDataCon freeConName
    Just foldFreeName <- liftCoreM $ CoreMonad.thNameToGhcName 'Free.foldFree
    foldF <- liftCoreM $ lookupId foldFreeName
    Just runCompName <- liftCoreM $ CoreMonad.thNameToGhcName 'Free.runComp
    runComp <- liftCoreM $ lookupId runCompName
    Just runCodName <- liftCoreM $ CoreMonad.thNameToGhcName 'Free.runCod
    runCod <- liftCoreM $ lookupId runCodName
    Just compName <- liftCoreM $ CoreMonad.thNameToGhcName 'Free.Comp
    comp <- liftCoreM $ lookupDataCon compName
    Just codName <- liftCoreM $ CoreMonad.thNameToGhcName 'Free.Codensity
    cod <- liftCoreM $ lookupDataCon codName
    Just plusName <- liftCoreM $ CoreMonad.thNameToGhcName ''(+)
    plus <- liftCoreM $ lookupTyCon plusName
    Just functorName <- liftCoreM $ CoreMonad.thNameToGhcName ''Functor
    fun <- liftCoreM $ lookupTyCon functorName
    Just fmapName <- liftCoreM $ CoreMonad.thNameToGhcName 'fmap
    fm <- liftCoreM $ lookupId fmapName
    Just applicativeName <- liftCoreM $ CoreMonad.thNameToGhcName ''Applicative
    app <- liftCoreM $ lookupTyCon applicativeName
    Just monadName <- liftCoreM $ CoreMonad.thNameToGhcName ''Monad
    mon <- liftCoreM $ lookupTyCon monadName
    Just returnName <- liftCoreM $ CoreMonad.thNameToGhcName 'return
    ret <- liftCoreM $ lookupId returnName
    Just bindName <- liftCoreM $ CoreMonad.thNameToGhcName '(>>=)
    bind <- liftCoreM $ lookupId bindName

    let Just fClass = tyConClass_maybe fun
        Just appClass = tyConClass_maybe app
        Just mClass = tyConClass_maybe mon

    let plusVars = tyConTyVars plus
        plusFunctorInner = (mkTyConApp plus (map mkTyVarTy (take (length plusVars - 1) plusVars)))
    instPlus <- liftCoreM $ findInstance mg fClass [plusFunctorInner]

    let fTy = Var.varType f
        (forAlls, rest) = Type.splitForAllTys fTy
        (argTys , rTy) = Type.splitFunTys rest
        (fTyBinders, fValBinders, body') = CoreSyn.collectTyAndValBinders body
    (rTyCon, _rTyArgs) <- liftMaybe "Return Free has no TyCon" $
        Type.splitTyConApp_maybe rTy

    let bMap = binderMap' forAlls (takeWhile isClassPred argTys) (fTyBinders ++ fValBinders) 
    let dictLookup = findDictionary mg plus forAlls (takeWhile isClassPred argTys) (fTyBinders ++ fValBinders)

    unless (rTyCon == freeTyCon) $
        fail $ "return is not free " ++ dump rTyCon

    let realArgs = dropWhile isClassPred argTys
        origInFree = head $ dropWhile isClassPred argTys
        inFree = bMap $ origInFree
    
    -- TODO there's double work being done here as inFree has already been bmapped
    let retFree         = (tail (dropWhile isClassPred argTys)) ++ [rTy]
        inA             = bMap $ head (tail (snd (splitTyConApp inFree)))
        outA            = bMap $ head (tail (snd (splitTyConApp (last retFree))))
        origInG         = head (snd (splitTyConApp origInFree))
        inG             = bMap $ origInG
        origOutG        = head (snd (splitTyConApp (last retFree)))
        outG            = bMap $ origOutG
        (lets, inner)   = splitDicts body'
        innerLams       = fst (splitLams inner)
        newfValBinders  = take (length $ takeWhile isClassPred argTys) $ fValBinders ++ innerLams

    valBindersFromY <- fillValBinders (dropWhile isClassPred argTys) (drop (length $ takeWhile isClassPred argTys) fValBinders ++ innerLams)

    let inY             = head valBindersFromY


    let returnFree = bMap $ (\(fr, rs) -> mkTyConApp fr (take (length rs - 1) rs)) (splitTyConApp (last retFree))

    liftCoreM $ Outputable.pprTrace "fTyBinders " (Outputable.ppr fTyBinders) $ return ()
    liftCoreM $ Outputable.pprTrace "fValBinders " (Outputable.ppr fValBinders) $ return ()
    liftCoreM $ Outputable.pprTrace "inFree " (Outputable.ppr inFree) $ return ()
    liftCoreM $ Outputable.pprTrace "retFree " (Outputable.ppr retFree) $ return ()
    liftCoreM $ Outputable.pprTrace "inA " (Outputable.ppr inA) $ return ()
    liftCoreM $ Outputable.pprTrace "outA " (Outputable.ppr outA) $ return ()
    liftCoreM $ Outputable.pprTrace "origInG " (Outputable.ppr origInG) $ return ()
    liftCoreM $ Outputable.pprTrace "inG " (Outputable.ppr inG) $ return ()
    liftCoreM $ Outputable.pprTrace "origOutG " (Outputable.ppr origOutG) $ return ()
    liftCoreM $ Outputable.pprTrace "outG " (Outputable.ppr outG) $ return ()
    liftCoreM $ Outputable.pprTrace "inY " (Outputable.ppr inY) $ return ()

    -- determine carrier
    let insideFunctor = fmap bMap $ findInsideFunctor inA outA
        outsideFunctor = fmap bMap $ findOutsideFunctor retFree

    liftCoreM $ Outputable.pprTrace "insideFunctor " (Outputable.ppr insideFunctor) $ return ()
    liftCoreM $ Outputable.pprTrace "outsideFunctor " (Outputable.ppr outsideFunctor) $ return ()

    -- split in generator and algebra
    
    (gen, alg) <- findGenAlg varDataCon conDataCon foldF inner

    abstrVars <- mkAbstrVars returnFree outG
    let (mkInsideComp, mkUnInsideComp) = makeInsideComps comp runComp insideFunctor abstrVars inA
        (mkOutsideComp, mkUnOutsideComp) = makeOutsideComps comp runComp outsideFunctor abstrVars inA

    let codInnerMaker' = (\inner -> maybe inner (\iF -> mkTyConApp (dataConTyCon comp) [inner, iF]) insideFunctor)
        codInnerMaker = (\inner -> maybe (codInnerMaker' inner) (\oF -> mkTyConApp (dataConTyCon comp) [oF, codInnerMaker' inner]) outsideFunctor)
        codInner = codInnerMaker (mkTyVarTy (cVar abstrVars))
        codCab = newTyConCo (dataConTyCon cod)
        codType = mkTyConApp (dataConTyCon cod) [codInner, inA]
        (mkCod, mkUnCod) = mkCodWrappers cod runCod codInner inA
        inFoldType' = maybe inA (\iF -> mkAppTy iF inA) insideFunctor
        inFoldType'' = mkAppTy (mkTyVarTy (cVar abstrVars)) inFoldType'
        inFoldType = maybe inFoldType'' (\oF -> mkAppTy oF inFoldType'') outsideFunctor

    -- replace recursive calls by their results (id)


    gen' <- removeRecursion gen f fm inFree inFoldType
    alg' <- removeRecursion alg f fm inFree inFoldType

    -- abstract var con bind
    
    genRep <- replacevcb varDataCon conDataCon (varv abstrVars) (conv abstrVars) (bindv abstrVars) gen'
    algRep <- replacevcb varDataCon conDataCon (varv abstrVars) (conv abstrVars) (bindv abstrVars) alg'
    fakeVar <- liftCoreM $ freshTyVar "a" (typeKind inA) --TODO maybe replace this by the actual var....
    let agen = mkCoreLams (sequence [varv, conv, bindv] abstrVars) (mkCoreLams [fakeVar] genRep)
        aalg = mkCoreLams (sequence [varv, conv, bindv] abstrVars) (mkCoreLams [fakeVar] algRep)


    -- wrap gen in carrier
    inGDict <- liftCoreM $ dictLookup fClass origInG 
    outGDict <- liftCoreM $ dictLookup fClass origOutG
    liftCoreM $ Outputable.pprTrace "inGDict " (Outputable.ppr inGDict) $ return ()
    liftCoreM $ Outputable.pprTrace "outGDict " (Outputable.ppr outGDict) $ return ()

    let removedType = head (snd (splitAppTys inG)) -- Assumes incoming free of form (__ + g)
    instRemoved' <- liftCoreM $ findInstance mg fClass [removedType]
    let instRemoved = mkTyApps (Var instRemoved') $ snd (splitAppTys removedType)

    -- TODO what if the right side is Void? that should work too...
    --boundG <- findRightVar fTyBinders (typeKind outG) -- TODO fix this
    --gDict <- findRightVar newfValBinders (mkTyConApp fun [outG]) -- Assumes outgoing of form g
    --liftCoreM $ Outputable.pprTrace "gDict " (Outputable.ppr gDict) $ return ()


    --let fInst = (App (App (App (App (Var instPlus) (Type removedType)) (Type outG)) instRemoved) (Var gDict))

    let fullReturnFree = (bMap (mkFunTys (init retFree) (last retFree)))
    --liftCoreM $ Outputable.pprTrace "replace " (Outputable.ppr fullReturnFree) $ return ()

        
    --liftCoreM $ Outputable.pprTrace "replace " (Outputable.ppr inFree) $ return ()
    --liftCoreM $ Outputable.pprTrace "by " (Outputable.ppr inFoldType) $ return ()

    let shortReturnFree = (bMap (last retFree))
    --liftCoreM $ Outputable.pprTrace "replace " (Outputable.ppr shortReturnFree) $ return ()
    --liftCoreM $ Outputable.pprTrace "by " (Outputable.ppr inFoldType'') $ return ()

    

    newIn <- mapM (\i -> liftCoreM $ freshVar (occNameString (getOccName i)) (varType i)) (tail valBindersFromY)


    let (ll, insideGen) = splitLams agen
        newerGen = mkCoreLams ll (mkOutsideComp (mkInsideComp (mkCoreLams newIn insideGen)))
        gen''' = mkCoreLams ((cVar abstrVars) : fTyBinders ++ newfValBinders) newerGen
        gen'' = replaceType shortReturnFree inFoldType'' $ replaceType fullReturnFree inFoldType $ replaceType inFree inFoldType gen'''
        


    -- unwrap and rewrap alg
    let newOpType = mkAppTy inG codType

    newOp  <- liftCoreM $ freshVar "op" newOpType
    anyVar <- liftCoreM $ freshVar "m" codType
    newK   <- liftCoreM $ freshVar "k" (mkFunTy inA (mkAppTy codInner inA))

    let idF = mkCoreLams [anyVar] (mkUnInsideComp (mkUnOutsideComp (mkUnCod (Var newK) (Var anyVar))))
        outOfFmap = inA
        outOfFmap' = if isJust insideFunctor
                        then mkAppTy (fromJust insideFunctor) outOfFmap
                        else outOfFmap
        outOfFmap'' = mkAppTy (mkTyVarTy (cVar abstrVars)) outOfFmap'
        outOfFmap''' = if isJust outsideFunctor
                            then mkAppTy (fromJust outsideFunctor) outOfFmap''
                            else outOfFmap''

    liftCoreM $ Outputable.pprTrace "outOfFmap" (Outputable.ppr outOfFmap''') $ return ()

    let leFmap = mkCoreApps (Var fm) [Type inG, inGDict, Type codType, Type outOfFmap''', idF, Var newOp]

    liftCoreM $ Outputable.pprTrace "leFmap" (Outputable.ppr leFmap) $ return ()

    let sub = extendSubstList emptySubst $ zip (tail valBindersFromY) (map Var newIn)
        aalg' = GhcPlugins.substExpr empty sub aalg

    liftCoreM $ Outputable.pprTrace "aalg'" (Outputable.ppr aalg') $ return ()
    let alg'''  = mkCoreLams ((cVar abstrVars) : fTyBinders ++ newfValBinders) $ rewrap newOp newK newIn mkOutsideComp mkCod mkInsideComp leFmap aalg'
    liftCoreM $ Outputable.pprTrace "alg'''" (Outputable.ppr alg''') $ return ()

    let alg'' = replaceType shortReturnFree inFoldType'' $ replaceType fullReturnFree inFoldType $ replaceType inFree inFoldType alg'''

    liftCoreM $ Outputable.pprTrace "alg''" (Outputable.ppr alg'') $ return ()

    -- These need to be globVar if they are to be separate bindings
    genVar <- liftCoreM $ freshVar "gen" (exprType gen'')
    algVar <- liftCoreM $ freshVar "alg" (exprType alg'')

    -- make replacement function
    let codTyCon = dataConTyCon cod
        codVars = tyConTyVars codTyCon
        codMonadInner = (mkTyConApp codTyCon [mkTyVarTy (head codVars)])
    instCodFun <- liftCoreM $ findInstance mg fClass [codMonadInner]
    instCodApp <- liftCoreM $ findInstance mg appClass [codMonadInner]
    instCodMon <- liftCoreM $ findInstance mg mClass [codMonadInner]

    let codTypeMaker = (\inner a -> mkTyConApp (dataConTyCon cod) [codInnerMaker inner, a])
        freeCodType = codTypeMaker returnFree inA 
        codMiniTypeMaker = (\inner -> mkTyConApp (dataConTyCon cod) [codInnerMaker inner])

    let codFunDictMaker = (\inner -> mkTyApps (Var instCodFun) [codInnerMaker inner])
        codAppDictMaker = (\inner -> (mkCoreApps (mkTyApps (Var instCodApp) [codInnerMaker inner]) [codFunDictMaker inner]))
        codMonDictMaker = (\inner -> (mkCoreApps (mkTyApps (Var instCodMon) [codInnerMaker inner]) [codAppDictMaker inner]))
        codMon = codMonDictMaker returnFree

    let ftc = freeTyCon
        ftcVars = tyConTyVars ftc
        fMonInner = (mkTyConApp ftc $ map mkTyVarTy (take (length ftcVars - 1) ftcVars))
    instFreeFun <- liftCoreM $ findInstance mg fClass [fMonInner]
    instFreeApp <- liftCoreM $ findInstance mg appClass [fMonInner]
    instFreeMon <- liftCoreM $ findInstance mg mClass [fMonInner]

    let freeGFun = (mkCoreApps (mkTyApps (Var instFreeFun) [outG]) [outGDict])
        freeGApp = (mkCoreApps (mkTyApps (Var instFreeApp) [outG]) [freeGFun, outGDict])
        freeGMon = (mkCoreApps (mkTyApps (Var instFreeMon) [outG]) [freeGApp, outGDict])

    let leReturn = (mkCoreApps (mkTyApps (Var ret) [returnFree]) [freeGMon])
        leCon = (mkTyApps (Var (dataConWorkId conDataCon)) [outG])
        leBind = (mkCoreApps (mkTyApps (Var bind) [returnFree]) [freeGMon])
        staticAlg = (mkCoreApps (mkTyApps (Var algVar) (returnFree : map mkTyVarTy fTyBinders)) (map Var newfValBinders))
        filledAlg = (mkCoreApps staticAlg [leReturn, leCon, leBind, Type inA])
        abstractAlg = (\v c b -> mkCoreApps (mkCoreApps (mkTyApps alg'' (returnFree : map mkTyVarTy fTyBinders)) (map Var newfValBinders)) [v, c, b])
        staticGen = (mkCoreApps (mkTyApps (Var genVar) (returnFree : map mkTyVarTy fTyBinders)) (map Var newfValBinders))
        filledGen = (mkCoreApps staticGen [leReturn, leCon, leBind, Type inA])
        abstractGen = (\v c b -> mkCoreApps (mkCoreApps (mkTyApps gen'' (returnFree : map mkTyVarTy fTyBinders)) (map Var newfValBinders)) [v, c, b])
        fullFold = \insideType -> (mkCoreApps (mkTyApps (Var foldF) [inA, insideType, inG]) [inGDict])
        theReturn = (mkTyApps (mkCoreApp (mkTyApps (Var ret) [freeCodType]) codMon) [inA])
        appFold = (mkCoreApps (fullFold freeCodType) [theReturn, filledAlg, Var inY])
        abstractFold = (\insideType ret alg body -> (mkCoreApps (fullFold insideType) [ret, alg, body]))
        codRunner = (\innerType gen alg -> (mkCoreApps (mkTyApps (Var runCod) [inA, (codInnerMaker innerType), inA]) [gen, alg]))
        unComp1 = if isJust insideFunctor 
                    then (\innerType gen alg -> (mkCoreApps (mkTyApps (Var runComp) [innerType, fromJust insideFunctor, inA]) [codRunner innerType gen alg]))
                    else codRunner -- TODO replace by maybe
        unComp2 = if isJust outsideFunctor
                    then (\innerType gen alg -> (mkCoreApps (mkTyApps (Var runComp) [fromJust outsideFunctor, innerType, inA]) [unComp1 innerType gen alg]))
                    else unComp1
        body'' = mkCoreApps (unComp2 returnFree filledGen appFold) (map Var (tail valBindersFromY))
        

    let body' = wrapNewBody (fTyBinders ++ newfValBinders ++ valBindersFromY) lets genVar gen'' algVar alg'' body''

    liftCoreM $ Outputable.pprTrace "body' " (Outputable.ppr body') $ return ()

    liftCoreM $ Outputable.pprTrace "freeGMon " (Outputable.ppr freeGMon) $ return ()
    -- TODO we're throwing away the lets, this is probably not always safe...
    return $ FullInfo
                { body = body'
                , nTyBinders = length (fTyBinders ++ newfValBinders)
                , binders = fTyBinders ++ newfValBinders ++ valBindersFromY
                , codTypeMaker = codMiniTypeMaker
                , codMonDictMaker = codMonDictMaker
                , gen = gen''
                , alg = alg''
                , fold = abstractFold
                , returnFree = returnFree
                , returnMonDict = freeGMon
                , outG = outG
                , inA = inA
                , unwrapper = unComp2
                }


wrapNewBody :: [Var] -> (CoreExpr -> CoreExpr) -> Var -> CoreExpr -> Var -> CoreExpr -> CoreExpr -> CoreExpr
wrapNewBody binders lets genVar gen algVar alg innerBody = 
    mkCoreLams binders
        (lets (Let (NonRec genVar gen)
                (Let (Rec [(algVar, alg)])
                    innerBody)))


-- Splits a coreexpr in a series of let-expressions and their body
splitDicts :: CoreExpr -> (CoreExpr -> CoreExpr, CoreExpr)
splitDicts (Let b e) = let (lets, inner) = splitDicts e
                        in (\x -> mkCoreLet b (lets x), inner)
splitDicts x = (id, x)


-- Splits a CoreExpr in a generator and algebra
-- Throws away leading lambdas (TODO fix this - is this even a problem?)
findGenAlg :: DataCon -> DataCon -> Var -> CoreExpr -> RewriteM (CoreExpr, CoreExpr)
findGenAlg var con ff (Lam b x) = findGenAlg var con ff x
findGenAlg var con ff (Case bs b t alts) = let Just (_, varParams, varBody) = (find (\(tc, _, _) -> tc == DataAlt var) alts)
                                 in let Just (_, conParams, conBody) = (find (\(tc, _, _) -> tc == DataAlt con) alts)
                                  in return $ (mkCoreLams varParams varBody, mkCoreLams conParams conBody)
findGenAlg var con ff (App x y) = 
    let (f, e) = collectArgs (App x y)
    in case f of
        Var v -> if v == ff 
                 then return (e !! 4, e !! 5) 
                 else fail $ "Didn't find it man"
        _ -> fail $ "Not here"
findGenAlg var con ff _ = fail $ "Wrong form"

-- Replaces var con, return and bind by their replacements
-- TODO remove excessive debugging info
replacevcb :: DataCon -> DataCon -> Var -> Var -> Var -> CoreExpr -> RewriteM CoreExpr
replacevcb varDataCon conDataCon var con bind e = 
    --(liftCoreM $ Outputable.pprTrace "Replace vcb any " (Outputable.ppr e) $ return ()) >>
    recursiveT
        (\x y ->
        --(liftCoreM $ Outputable.pprTrace "Replace vcb app " (Outputable.ppr x) $ return ()) >>
        let (app, args) = CoreSyn.collectArgs (App x y)
        in case app of
            (Var v) -> 
                case (idToDataCon v) of
                        Just dc -> if (dc == varDataCon) then do 
                                    { liftCoreM $ Outputable.pprTrace "found Var" (Outputable.ppr args) $ return () 
                                    ; (lams, x') <- if length args == 2 then do
                                                { newV <- liftCoreM $ freshVar "x" (extractType $ args !! 1)
                                                ; return (\x -> mkCoreLams [newV] x, Var newV)}
                                                else do
                                                   { x'' <- (replacevcb varDataCon conDataCon var con bind (args !! 2))
                                                   ; return (id, x'')}
                                    ; x  <- return $ lams (mkCoreApps (Var var) ([args !! 1, x']))
                                    ; liftCoreM $ Outputable.pprTrace "ended var" (Outputable.ppr x) $ return ()
                                    ; return x
                                    }
                                        else if (dc == conDataCon) then do
                                        { liftCoreM $ Outputable.pprTrace "found Con" (Outputable.ppr args) $ return ()
                                        ; (lams, x') <- if length args == 2 then do
                                                    { newV <- liftCoreM $ freshVar "x" (extractType $ args !! 1)
                                                    ; return (\x -> mkCoreLams [newV] x, Var newV)}
                                                    else do
                                                       { x'' <- (replacevcb varDataCon conDataCon var con bind (args !! 2))
                                                       ; return (id, x'')}
                                        ; x  <- return $ lams (mkCoreApps (Var con) [args !! 1, x'])
                                        ; liftCoreM $ Outputable.pprTrace "ended con" (Outputable.ppr x) $ return ()
                                        ; return x
                                        }
                                        else do {
                                              liftCoreM $ Outputable.pprTrace "Found other dc" (Outputable.ppr dc) $ return ();
                                              liftCoreM $ Outputable.pprTrace "Reworking args" (Outputable.ppr args) $ return ();
                                              --x' <- replacevcb varDataCon conDataCon var con bind app; 
                                              args' <- mapM (replacevcb varDataCon conDataCon var con bind) args;
                                              liftCoreM $ Outputable.pprTrace "Reworked args" (Outputable.ppr args') $ return ();
                                              liftCoreM $ Outputable.pprTrace "apped" (Outputable.ppr (mkCoreApps app args')) $ return ();
                                              
                                              return $ mkCoreApps app args' }
                        Nothing -> if (occNameString (getOccName v) == ">>=") -- TODO dont replace all binds!
                                    then do {
                                            m <- replacevcb varDataCon conDataCon var con bind (args !! 4);
                                            f' <- replacevcb varDataCon conDataCon var con bind (args !! 5);
                                            return $ mkCoreApps (Var bind) ([args !! 2, args !! 3, m, f']) }
                                    else if (occNameString (getOccName v) == "return") -- TODO dont replace all returns
                                           then replacevcb varDataCon conDataCon var con bind (args !! 3) >>= \x' -> return $ mkCoreApps (Var var) ([args !! 2, x'])
                                           else do {
                                              liftCoreM $ Outputable.pprTrace "Found other var" (Outputable.ppr v) $ return ();
                                              x' <- replacevcb varDataCon conDataCon var con bind x; 
                                              y' <- replacevcb varDataCon conDataCon var con bind y;
                                              return $ mkCoreApp x' y' }
            _ -> do {
                  x' <- replacevcb varDataCon conDataCon var con bind x; 
                  y' <- replacevcb varDataCon conDataCon var con bind y;
                  return $ mkCoreApp x' y' }
        )
        e

-- replaces calls to f by their input (which is the result in a fold)
removeRecursion :: CoreExpr -> Var -> Var -> Type -> Type -> RewriteM CoreExpr
removeRecursion e f fm inT outT =
    recursiveT
        (\x y -> 
        let (app, args) = CoreSyn.collectArgs (App x y)
        in case app of
            (Var v) -> let realargs = dropWhile typeOrDict args
                          in if (v == f) 
                                then case length realargs of
                                    0 -> do { newArg <- liftCoreM $ freshVar "x" anyKind;
                                                return (Lam newArg (Var newArg)) }
                                    1 -> removeRecursion (head realargs) f fm inT outT >>= \x -> return $ replaceType inT outT x
                                    _ -> do { x' <- removeRecursion (head realargs) f fm inT outT;
                                              liftCoreM $ Outputable.pprTrace "recrem" (Outputable.ppr x') $ return ();
                                              x'' <- return $ replaceType inT outT x';
                                              liftCoreM $ Outputable.pprTrace "recrem" (Outputable.ppr x'') $ return ();
                                              mapM (\x -> removeRecursion x f fm inT outT >>= return . replaceType inT outT) (tail realargs)
                                                >>= (\ys -> return $ mkCoreApps x'' ys)}
                            else if (v == fm)
                                then case head realargs of
                                    App x' y' -> 
                                        let (app', args') = collectArgs (head realargs)
                                        in case app' of
                                        Var v' -> if v' == f
                                                    then removeRecursion (realargs !! 1) f fm inT outT
                                                    else mapM (\x -> removeRecursion x f fm inT outT) args
                                                                >>= (\ys -> return $ mkCoreApps (Var v) ys)
                                        _ -> mapM (\x -> removeRecursion x f fm inT outT) args
                                                    >>= (\ys -> return $ mkCoreApps (Var v) ys)
                                    _ -> mapM (\x -> removeRecursion x f fm inT outT) args
                                                >>= (\ys -> return $ mkCoreApps (Var v) ys)
                                else do { x' <- removeRecursion x f fm inT outT;
                                          y' <- removeRecursion y f fm inT outT;
                                          return $ mkCoreApp x' y'}
            _ -> do { x' <- removeRecursion x f fm inT outT;
                      y' <- removeRecursion y f fm inT outT;
                      return $ mkCoreApp x' y'}
        )
        e

varTy :: TyVar -> TyVar -> Type
varTy a c = mkForAllTy a (mkFunTy (mkTyVarTy a) (mkAppTy (mkTyVarTy c) (mkTyVarTy a)))

conTy :: Type -> TyVar -> TyVar -> Type
conTy g a c = mkForAllTy a (mkFunTy
                (mkAppTy g (mkAppTy (mkTyVarTy c) (mkTyVarTy a)))
                (mkAppTy (mkTyVarTy c) (mkTyVarTy a)))

bindTy :: TyVar -> TyVar -> TyVar -> Type
bindTy a b c = mkForAllTys [a,b] (mkFunTys
                [mkAppTy (mkTyVarTy c) (mkTyVarTy a)
                , mkFunTy (mkTyVarTy a) (mkAppTy (mkTyVarTy c) (mkTyVarTy b))]
                (mkAppTy (mkTyVarTy c) (mkTyVarTy b)))

typeOrDict :: CoreExpr -> Bool
typeOrDict e = case e of
    Type t -> True
    Var x -> isDictLikeTy (varType x)
    _ -> False

splitvcb :: CoreExpr -> (CoreExpr -> CoreExpr, CoreExpr)
splitvcb e = case e of
    Lam v (Lam c (Lam b x)) -> (\y -> (Lam v (Lam c (Lam b y))), x)
    _ -> undefined

rewrap :: Var -> Var -> [Var] -> (CoreExpr -> CoreExpr) -> (CoreExpr -> CoreExpr) -> (CoreExpr -> CoreExpr) -> CoreExpr -> CoreExpr -> CoreExpr
rewrap newOp newK ins mkOutComp mkCod mkComp leFmap e = 
    let (lams, inner) = splitLams e
        inner' = if null ins then inner else mkCoreLams ins inner
        fmapAndInner = (mkCoreApp (mkCoreLams [last lams] inner') leFmap)
        inK = (mkOutComp (mkComp fmapAndInner))
       in mkCoreLams (take (length lams - 1) lams ++ [newOp]) 
        (mkCod (mkCoreLams [newK] 
            inK))

addLam :: Var -> CoreExpr -> CoreExpr
addLam newLam e = let (lams, inner) = splitLams e
                    in mkCoreLams (lams ++ [newLam]) inner

findInsideFunctor :: Type -> Type -> Maybe Type
findInsideFunctor inA outA = do
    (tc, ts) <- splitTyConApp_maybe outA
    return $ mkTyConApp tc (init ts)

findOutsideFunctor :: [Type] -> Maybe Type
findOutsideFunctor retFree = 
    case length $ init retFree of
        0 -> Nothing
        _ -> let (f, rs) = splitAppTys $ mkFunTys (init retFree) (last retFree)
             in Just $ mkAppTys f (init rs)

findInstance :: ModGuts -> Class -> [GhcPlugins.Type] -> CoreM DFunId
findInstance guts cls args = do
  hsc_env <- getHscEnv
  (_, mb_clsInst) <- liftIO $ initTcFromModGuts hsc_env guts HsSrcFile False $
                  tcLookupInstance cls args
  return $ maybe (panic "No instance found") instanceDFunId mb_clsInst

data AbstractVars = AbstractVars
    { aVar :: Var
    , bVar :: Var
    , cVar :: Var
    , varv :: Var
    , conv :: Var
    , bindv :: Var
    }

mkAbstrVars :: Type -> Type -> RewriteM AbstractVars
mkAbstrVars returnFree outG = do
    aVar <- liftCoreM $ freshTyVar "a" liftedTypeKind
    bVar <- liftCoreM $ freshTyVar "b" liftedTypeKind
    cVar <- liftCoreM $ freshTyVar "c" $ typeKind returnFree
    varv <- liftCoreM $ (freshVar "var" (varTy aVar cVar))
    conv <- liftCoreM $ (freshVar "con" (conTy outG aVar cVar))
    bindv <- liftCoreM $ (freshVar "bind" (bindTy aVar bVar cVar))
    return AbstractVars 
        { aVar = aVar
        , bVar = bVar
        , cVar = cVar
        , varv = varv
        , conv = conv
        , bindv = bindv
        }

fillValBinders :: [Type] -> [Var] -> RewriteM [Var]
fillValBinders types vars = do
    let restTypes = drop (length vars) types
    newVars <- liftCoreM $ mapM (freshVar "newIn") restTypes
    return $ vars ++ newVars


binderMap' :: [TyVar] -> [PredType] -> [Var] -> Type -> Type
binderMap' foralls preds vars ty = maybe
                                    (binderMap foralls preds vars ty)
                                    (\(t1, t2) -> mkAppTy (binderMap' foralls preds vars t1) (binderMap' foralls preds vars t2))
                                    (splitAppTy_maybe ty)

binderMap :: [TyVar] -> [PredType] -> [Var] -> Type -> Type
binderMap foralls preds vars ty = maybe ty mkTyVarTy (binderMapVar foralls preds vars ty)

binderMapVar :: [TyVar] -> [PredType] -> [Var] -> Type -> Maybe TyVar
binderMapVar foralls preds vars ty =
    let fam = getTyVar_maybe ty >>= flip elemIndex foralls >>= \i -> return $ (vars !! i)
    in if isJust fam then fam
        else let pm = findIndex (eqPred ty) preds >>= \i -> return $ (vars !! (length foralls + i))
            in pm

findRightVar :: [Var] -> Type -> RewriteM Var
findRightVar binders typ = do
    liftCoreM $ Outputable.pprTrace "findG" (Outputable.ppr (map varType binders)) $ return ()
    liftCoreM $ Outputable.pprTrace "findGType" (Outputable.ppr typ) $ return ()
    maybe
        (fail "No right var")
        return
        $ find (\v -> varType v == typ) binders

--------------------------------------------------------------------------------

makeInsideComps :: DataCon -> Id -> Maybe Type -> AbstractVars -> Type -> (CoreExpr -> CoreExpr, CoreExpr -> CoreExpr)
makeInsideComps comp runComp insideFunctor abstrVars inA =
   let mkC   = maybe id (\iF -> (\innerExpr -> (mkCoreApp (mkCoreConApps comp (map Type [mkTyVarTy $ cVar abstrVars, iF, inA]))) innerExpr)) insideFunctor
       mkUnC = maybe id (\iF -> (\innerExpr -> (mkCoreApp (mkTyApps (Var runComp) [mkTyVarTy $ cVar abstrVars, iF, inA]) innerExpr))) insideFunctor
   in (mkC, mkUnC)

makeOutsideComps :: DataCon -> Id -> Maybe Type -> AbstractVars -> Type -> (CoreExpr -> CoreExpr, CoreExpr -> CoreExpr)
makeOutsideComps comp runComp outsideFunctor abstrVars inA =
   let mkC   = maybe id (\oF -> (\innerExpr -> (mkCoreApp (mkCoreConApps comp (map Type [oF, mkTyVarTy $ cVar abstrVars, inA])) innerExpr))) outsideFunctor
       mkUnC = maybe id (\oF -> (\innerExpr -> (mkCoreApp (mkTyApps (Var runComp) [oF, mkTyVarTy $ cVar abstrVars, inA]) innerExpr))) outsideFunctor
   in (mkC, mkUnC)

mkCodWrappers :: DataCon -> Id -> Type -> Type -> (CoreExpr -> CoreExpr, CoreExpr -> CoreExpr -> CoreExpr)
mkCodWrappers cod runCod codInner inA =
    let mkC = (\innerExpr -> (mkCoreApp (mkCoreConApps cod (map Type [codInner, inA])) innerExpr))
        mkUnC = (\gen alg -> (mkCoreApps (mkTyApps (Var runCod) [inA, codInner, inA]) [gen, alg]))
    in (mkC, mkUnC)


--------------------------------------------------------------------------------

-- TODO what if the type is an app on the left side? would splitAppTy make this invisible?
replaceTypeType :: Type -> Type -> Type -> Type
replaceTypeType inT outT t =
    if t == inT then outT
     else let (fT, rt) = splitFunTys t
           in let rec = (\t -> if t == inT then outT else maybe t (\(t1, t2) -> mkAppTy (replaceTypeType inT outT t1) (replaceTypeType inT outT t2)) (splitAppTy_maybe t))
              in mkFunTys (map rec fT) (rec rt)

replaceType :: Type -> Type -> CoreExpr -> CoreExpr
replaceType inT outT e = case e of
    Type t -> Type $ replaceTypeType inT outT t
    Var v -> Var $ replaceTypeVar inT outT v
    Lit l -> Lit l
    App x y -> mkCoreApp (replaceType inT outT x) (replaceType inT outT y)
    Let b x -> Let (replaceTypeBind inT outT b) (replaceType inT outT x)
    Lam v x -> Lam (replaceTypeVar inT outT v) (replaceType inT outT x)
    Cast b c -> Cast (replaceType inT outT b) c
    Tick t b -> Tick t (replaceType inT outT b)
    Coercion c -> Coercion c
    Case e' b t as -> Case (replaceType inT outT e')
                           (replaceTypeVar inT outT b)
                           (replaceTypeType inT outT t)
                           (map (\(ac, b, e) -> (ac, map (replaceTypeVar inT outT) b, replaceType inT outT e)) as)

replaceTypeVar :: Type -> Type -> Var -> Var
replaceTypeVar inT outT v = setVarType v (replaceTypeType inT outT (varType v))


replaceTypeBind :: Type -> Type -> CoreBind -> CoreBind
replaceTypeBind inT outT (NonRec b e) = NonRec (replaceTypeVar inT outT b) (replaceType inT outT e)
replaceTypeBind inT outT (Rec bs) = Rec $ map (\(b, e) -> (replaceTypeVar inT outT b, replaceType inT outT e)) bs

--------------------------------------------------------------------------------

-- Reconstruct dictionary
findDictionary :: ModGuts -> TyCon -> [TyVar] -> [PredType] -> [Var] -> Class -> Type -> CoreM CoreExpr
findDictionary mg plus foralls preds binders cl t = do
    let lookupB = binderMapVar foralls preds binders 
        lookupF = findInstance mg cl
        plusVars = tyConTyVars plus
        plusInner = (mkTyConApp plus (map mkTyVarTy (init plusVars)))
    instPlus <- lookupF [plusInner]
    Outputable.pprTrace "hey" (Outputable.ppr (mkTyConApp (classTyCon cl) [t])) $ return ()
    maybe
        (maybe (fmap Var $ lookupF [t]) (return . Var) (lookupB (mkTyConApp (classTyCon cl) [t])))
        (\(tc, tys) -> if tc == plus
                        then (mapM (findDictionary mg plus foralls preds binders cl) tys) >>= \tys' -> return $ mkCoreApps (Var instPlus) $ (map (Type . binderMap' foralls preds binders) tys) ++ tys'
                        else fmap (\x -> mkTyApps (Var x) (map (binderMap' foralls preds binders) tys))  $ lookupF [(mkTyConApp tc (map mkTyVarTy (init (tyConTyVars tc))))])
        (splitTyConApp_maybe t)
