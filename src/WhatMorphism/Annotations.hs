--------------------------------------------------------------------------------
{-# LANGUAGE DeriveDataTypeable #-}
module WhatMorphism.Annotations
    ( RegisterFoldBuild (..)
    , RegisterFreeInstance (..)
    ) where


--------------------------------------------------------------------------------
import           Data.Data     (Data)
import           Data.Typeable (Typeable)


--------------------------------------------------------------------------------
data RegisterFoldBuild = RegisterFoldBuild
    { registerFold  :: String
    , registerBuild :: String
    } deriving (Data, Typeable, Show)

data RegisterFreeInstance = RegisterFreeInstance
    { rfi :: String
    } deriving (Data, Typeable, Show)
