{-# LANGUAGE TemplateHaskell, RecordWildCards #-}
--------------------------------------------------------------------------------
--TODO move stuff to util module
module WhatMorphism.FreeFusion
    ( FreeInfo (..)
    , FreeRegister (..)
    , runFusion
    ) where

--------------------------------------------------------------------------------

import GhcPlugins

import           Control.Monad
import           Control.Monad.Error   (catchError)
import           CoreMonad             as CoreMonad
import qualified Outputable            as Outputable
import           Data.Map              (Map)
import qualified Data.Map              as M

import Data.Maybe

--------------------------------------------------------------------------------

import           WhatMorphism.Expr
import           WhatMorphism.Dump
import           WhatMorphism.RewriteM
import           WhatMorphism.Free hiding (Var)
import qualified WhatMorphism.Free as Free
import           WhatMorphism.FreeUtils

--------------------------------------------------------------------------------


data FreeRegister = FreeRegister {
        reworkedFrees :: Map Var FreeInfo
        }

data FreeInfo
    = FullInfo
        { body :: CoreExpr
        , nTyBinders :: Int
        , binders :: [Var]
        , codTypeMaker :: Type -> Type
        , codMonDictMaker :: Type -> CoreExpr
        , gen  :: CoreExpr
        , alg  :: CoreExpr
        , fold :: Type -> CoreExpr -> CoreExpr -> CoreExpr -> CoreExpr
        , returnFree :: Type
        , returnMonDict :: CoreExpr
        , outG :: Type
        , gPos :: Maybe Int
        , inA :: Type
        , unwrapper :: Type -> CoreExpr -> CoreExpr -> CoreExpr
        }
    | EndInfo
        { body :: CoreExpr
        , abstractBody :: CoreExpr
        , inA :: Type }

--------------------------------------------------------------------------------

runFusion :: FreeRegister -> [CoreBind] -> RewriteM [CoreBind]
runFusion' fr = return
runFusion fr cbs = mapM fusion' cbs
    where
        fusion' cb =  
            (case cb of
                NonRec f e -> doFusion' f e >>= (\e' -> return $ NonRec f e')
                Rec bs -> liftM (Rec) $ mapM (\(f, e) -> doFusion' f e >>= \e' -> return $ (f, e')) bs)
        doFusion' f e = flip catchError (report f e) 
                        (do { message $ "doing fusion for " ++ dump f;
                              Just dotName <- liftCoreM $ CoreMonad.thNameToGhcName '(.);
                              dot <- liftCoreM $ lookupId dotName;
                              e' <- unrollDot dot e;
                              e'' <- fusionRework fr e';
                              return e''
                            })
        report f e err = do
            message $ "====== Error: " ++ err
            return e

--------------------------------------------------------------------------------

fusionRework :: FreeRegister -> CoreExpr -> RewriteM CoreExpr
fusionRework fr e = do 
    recursiveT (fusionApp fr) e

--------------------------------------------------------------------------------

fusionApp :: FreeRegister -> CoreExpr -> CoreExpr -> RewriteM CoreExpr
fusionApp fr x y =
    let (f, e') = collectArgs (App x y)
    in case f of 
         Var v -> if M.member v (reworkedFrees fr)
                    then case fromJust $ M.lookup v (reworkedFrees fr) of
                        fi@(FullInfo {..}) -> startFuse fr fi e'
                        EndInfo {..}-> do {x' <- fusionRework fr x; y' <- fusionRework fr y; return $ mkCoreApp x' y' }
                    else do { x' <- fusionRework fr x; y' <- fusionRework fr y; return $ mkCoreApp x' y' }
         _ -> do { x' <- fusionRework fr x; y' <- fusionRework fr y; return $ mkCoreApp x' y' }

--------------------------------------------------------------------------------

startFuse :: FreeRegister -> FreeInfo -> [CoreExpr] -> RewriteM CoreExpr
startFuse fr info args = do
    Just freeConName <- liftCoreM $ CoreMonad.thNameToGhcName 'Free.Con
    conDataCon <- liftCoreM $ lookupDataCon freeConName
    let insideType = returnFree info
        thisAlg = (mkTyApps (Var (dataConWorkId conDataCon)) [outG info])
        monDict = returnMonDict info
    fused <- appendFuse fr insideType monDict thisAlg info args
    liftCoreM $ Outputable.pprTrace "fused " (Outputable.ppr fused) $ return ()
    return fused

--------------------------------------------------------------------------------

appendFuse :: FreeRegister -> Type -> CoreExpr -> CoreExpr -> FreeInfo -> [CoreExpr] -> RewriteM CoreExpr
appendFuse fr insideType monDict insideAlg info args = do
    Just returnName <- liftCoreM $ CoreMonad.thNameToGhcName 'return
    ret <- liftCoreM $ lookupId returnName
    Just bindName <- liftCoreM $ CoreMonad.thNameToGhcName '(>>=)
    bind <- liftCoreM $ lookupId bindName
    Just freeConName <- liftCoreM $ CoreMonad.thNameToGhcName 'Free.Con
    conDataCon <- liftCoreM $ lookupDataCon freeConName

    let nextInsideType = (codTypeMaker info) insideType --(inA info)
        nextMonDict = (codMonDictMaker info) insideType

    let thisReturn = (mkCoreApp (mkTyApps (Var ret) [insideType]) monDict)
        thisCon = insideAlg
        thisBind = (mkCoreApp (mkTyApps (Var bind) [insideType]) monDict)


    let (lams, args') = if length args > nTyBinders info
                then ((take (nTyBinders info) (binders info)), args)
                else ((take (nTyBinders info + 1) (binders info)), (args ++ map Var (drop (length args) (take (nTyBinders info + 1) (binders info)))))
        
    let
        thisGen = mkCoreApps (mkCoreApps (mkTyApps (gen info) [insideType]) (take (nTyBinders info) args')) [thisReturn, thisCon, thisBind]
        thisAlg = mkCoreApps (mkCoreApps (mkTyApps (alg info) [insideType]) (take (nTyBinders info) args')) [mkTyApps thisReturn [inA info], thisCon, thisBind]


    insideBody <-let nextF = args' !! (nTyBinders info)
                     in case nextF of
                        (App x' y') -> let (f2, e2) = collectArgs nextF
                                        in case f2 of
                                            Var v2 -> if M.member v2 (reworkedFrees fr)
                                                        then case fromJust $ M.lookup v2 (reworkedFrees fr) of
                                                                fi'@(FullInfo {..}) -> appendFuse fr nextInsideType nextMonDict thisAlg fi' e2
                                                                fi'@(EndInfo {..}) -> makeEnd fr nextInsideType nextMonDict thisAlg fi' e2
                                                        else makeFold fr nextInsideType nextMonDict thisAlg info args'
                                            _ -> makeFold fr nextInsideType nextMonDict thisAlg info args'
                        _ -> makeFold fr nextInsideType nextMonDict thisAlg info args'

    liftCoreM $ Outputable.pprTrace "insideType" (Outputable.ppr insideType) $ return ()

    let fused = mkCoreApps 
                    --(mkCoreLams (take (nTyBinders info) (binders info))
                    (mkCoreLams lams
                    ((unwrapper info) 
                        insideType
                        thisGen
                        insideBody))
                    (except (nTyBinders info) args) -- TODO replace g arg by correct one...

    return fused

--------------------------------------------------------------------------------

makeFold :: FreeRegister -> Type -> CoreExpr -> CoreExpr -> FreeInfo -> [CoreExpr] -> RewriteM CoreExpr
makeFold fr insideType monDict insideAlg info args = do
    Just returnName <- liftCoreM $ CoreMonad.thNameToGhcName 'return
    ret <- liftCoreM $ lookupId returnName

    fullInside <- fusionRework fr (args !! nTyBinders info)
    
    liftCoreM $ Outputable.pprTrace "Alg " (Outputable.ppr insideAlg) $ return ()

    let nextReturn = (mkTyApps (mkCoreApp (mkTyApps (Var ret) [insideType]) monDict) [inA info])
        simpleFold = (fold info) insideType nextReturn insideAlg fullInside

    return simpleFold

--------------------------------------------------------------------------------

makeEnd :: FreeRegister -> Type -> CoreExpr -> CoreExpr -> FreeInfo -> [CoreExpr] -> RewriteM CoreExpr
makeEnd fr insideType monDict insideAlg info args = do
    liftCoreM $ Outputable.pprTrace "end args" (Outputable.ppr args) $ return ()
    Just returnName <- liftCoreM $ CoreMonad.thNameToGhcName 'return
    ret <- liftCoreM $ lookupId returnName
    Just bindName <- liftCoreM $ CoreMonad.thNameToGhcName '(>>=)
    bind <- liftCoreM $ lookupId bindName

    let nextReturn = (mkCoreApp (mkTyApps (Var ret) [insideType]) monDict)
        thisBind = (mkCoreApp (mkTyApps (Var bind) [insideType]) monDict)
        apped' = mkCoreApps (abstractBody info) [nextReturn, insideAlg, thisBind]
        apped = mkCoreApps apped' args

    liftCoreM $ Outputable.pprTrace "apped" (Outputable.ppr apped) $ return ()

    return apped

--------------------------------------------------------------------------------

except :: Int -> [a] -> [a]
except i l = take i l ++ drop (i + 1) l

--------------------------------------------------------------------------------

unrollDot :: Var -> CoreExpr -> RewriteM CoreExpr
unrollDot dot e = do
    e' <- recursiveT (unrollAppDot' dot) e
    return e'
    
-- Safe version (with mkCore -- doesnt work with -O2)
unrollAppDot' :: Var -> CoreExpr -> CoreExpr -> RewriteM CoreExpr
unrollAppDot' dot x y =
    let (f, e) = collectArgs (App x y)
    in case f of
        App x' y' -> let (f', e') = collectArgs (App x' y')
                        in case f' of
                            Var v -> if v == dot
                                        then do { e'' <- unrollDot dot $ mkCoreApps (e' !! 4) ((drop 5 e') ++ e);
                                                  return $ mkCoreApp (e' !! 3) e''}
                                        else do { x' <- unrollDot dot x; y' <- unrollDot dot y; return $ mkCoreApp x' y' }
                            _ -> do {x' <- unrollDot dot x; y' <- unrollDot dot y; return $ mkCoreApp x' y' }
        Var v -> if v == dot
                    then do { (lamW, inApp) <- if length e == 5
                                        then do { newV <- liftCoreM $ freshVar "any" (extractType (e !! 2));
                                                  return (Lam newV, [Var newV]) }
                                        else (return $ (id, drop 5 e));
                              e' <- unrollDot dot $ mkCoreApps (e !! 4) inApp;
                              return $ lamW (mkCoreApp (e !! 3) e')}
                    else do { x' <- unrollDot dot x; y' <- unrollDot dot y; return $ mkCoreApp x' y' }
        _ -> do {x' <- unrollDot dot x; y' <- unrollDot dot y; return $ mkCoreApp x' y' }

-- Unsafe version (with App)
unrollAppDot'' :: Var -> CoreExpr -> CoreExpr -> RewriteM CoreExpr
unrollAppDot'' dot x y =
    let (f, e) = collectArgs (App x y)
    in case f of
        App x' y' -> let (f', e') = collectArgs (App x' y')
                        in case f' of
                            Var v -> if v == dot
                                        then do { e'' <- unrollDot dot $ mkApps (e' !! 4) ((drop 5 e') ++ e);
                                                  return $ App (e' !! 3) e''}
                                        else do { x' <- unrollDot dot x; y' <- unrollDot dot y; return $ App x' y' }
                            _ -> do {x' <- unrollDot dot x; y' <- unrollDot dot y; return $ App x' y' }
        Var v -> if v == dot
                    then do { (lamW, inApp) <- if length e == 5
                                        then do { newV <- liftCoreM $ freshVar "any" (extractType (e !! 2));
                                                  return (Lam newV, [Var newV]) }
                                        else (return $ (id, drop 5 e));
                              e' <- unrollDot dot $ mkApps (e !! 4) inApp;
                              return $ lamW (App (e !! 3) e')}
                    else do { x' <- unrollDot dot x; y' <- unrollDot dot y; return $ App x' y' }
        _ -> do {x' <- unrollDot dot x; y' <- unrollDot dot y; return $ App x' y' }
