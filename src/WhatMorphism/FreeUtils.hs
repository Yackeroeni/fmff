module WhatMorphism.FreeUtils
    ( recursiveT
    , splitLams
    , extractType
    ) where

import GhcPlugins 

import WhatMorphism.RewriteM

-- Extracts the type from an expression, probably some function already exists for this, but I couldn't find it
extractType :: CoreExpr -> Type
extractType e = case e of
    Type t -> t
    _ -> anyKind

-- Recursively applies a function to Apps 
-- (This shouldn't really be in RewriteM, but it is easier to debug like that)
recursiveT :: (CoreExpr -> CoreExpr -> RewriteM CoreExpr) -> CoreExpr -> RewriteM CoreExpr
recursiveT f e = case e of
    Var x -> return $ Var x
    Lit x -> return $ Lit x
    Lam v' x -> recursiveT f x >>= return . Lam v'
    Let b' x -> do 
        b'' <- recursiveTBind f b'
        x' <- recursiveT f x
        return $ Let b'' x'
    Case x b t y -> do
        x' <- recursiveT f x
        y' <- mapM (\ (a,b,e) -> recursiveT f e >>= \e' -> return (a,b, e')) y
        return $ Case x' b t y'
    Cast e c -> recursiveT f e >>= \e' -> return $ Cast e' c
    Tick i e -> recursiveT f e >>= return . Tick i
    Type t -> return $ Type t
    Coercion c -> return $ Coercion c
    App x y -> f x y

-- Helper for recursiveT
recursiveTBind :: (CoreExpr -> CoreExpr -> RewriteM CoreExpr) -> CoreBind -> RewriteM CoreBind
recursiveTBind f (NonRec b e) = (recursiveT f e) >>= return . NonRec b 
recursiveTBind f (Rec bs) = do 
    bs' <- mapM (\(b, e) -> recursiveT f e >>= \e' -> return (b, e')) bs
    return $ Rec bs'


-- Splits the lambdas off an expression
splitLams :: CoreExpr -> ([Var], CoreExpr)
splitLams e = case e of
    Lam l x -> let (vs, e') = splitLams x in (l:vs, e')
    _ -> ([], e)
