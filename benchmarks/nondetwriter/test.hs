{-#LANGUAGE GADTs, RankNTypes, TypeOperators, UndecidableInstances,
 FlexibleInstances, FunctionalDependencies, FlexibleContexts, AllowAmbiguousTypes,
 DeriveFunctor #-}

import Data.Monoid
import WhatMorphism.Free
import Criterion.Main

--main = print $ (handleVoid . handleNondet . handleFail . handleWriter ) writingCoin
--main = print $ (handleVoid . handleNondet . handleFail . handleWriter)  writingCoin
--main = print $ handler (writingCoin 10)
main = defaultMain [
    bgroup "" [ bench "1" $ nf benchMe 1
                , bench "2" $ nf benchMe 2
                , bench "4" $ nf benchMe 4
                , bench "8" $ nf benchMe 8
                , bench "16" $ nf benchMe 16
                , bench "32" $ nf benchMe 32
                , bench "64" $ nf benchMe 64
                , bench "128" $ nf benchMe 128
                , bench "256" $ nf benchMe 256
                , bench "512" $ nf benchMe 512
                , bench "1024" $ nf benchMe 1024
                ]
    ]

benchMe :: Int -> [Maybe (String, Bool)]
benchMe n = handler (writingCoin n)

handler :: Monoid w => Free (Writer w + (Fail + (Nondet + Void))) a -> [Maybe (w, a)]
handler = handleVoid . handleNondet . handleFail . handleWriter

data Nondet k where
    Or :: k -> k -> Nondet k

instance Functor Nondet where
    fmap f (Or l r) = Or (f l) (f r)

instance (Show k) => Show (Nondet k) where
    show (Or l r) = "Or (" ++ show l ++ ") ("  ++ show r ++ ")"

data Fail k where
    Fail :: Fail k

instance Functor Fail where
    fmap _ Fail = Fail

instance Show (Fail k) where
    show Fail = "Fail"

data Void k

instance Functor Void 

data Writer w k where
    Tell :: w -> k -> Writer w k

instance Functor (Writer w) where
    fmap f (Tell w k) = Tell w (f k)

writingCoin :: Int -> Free (Writer String + (Fail + (Nondet + Void))) Bool
writingCoin 0 = Con (Inl (Tell " Done " (Var True)))
writingCoin n = Con (Inl (Tell " Rec " 
                             (Con (Inr (Inr (Inl (Or
                                (Con (Inr (Inl Fail)))
                                (Con (Inr (Inr (Inl (Or
                                    (Con (Inl (Tell (show n) (writingCoin (n - 1)))))
                                    (Con (Inl (Tell "False" (Var False))))
                                )))))
                              )))))
                        ))

{-# NOINLINE handleFail'' #-}
handleFail'' :: Functor g => Free (Fail + g) a -> Free g (Maybe a)
handleFail'' = foldFree
    (\x -> Var (Just x))
    (\op -> case op of
        Inl x -> case x of
            Fail -> Var Nothing
        Inr x -> Con x)

handleWriter :: (Monoid w, Functor g) => Free (Writer w + g) a -> Free g (w, a)
handleWriter = \y -> case y of
    Var x -> return (mempty, x)
    Con op -> case op of
        Inl x -> case x of
            Tell w1 k -> (handleWriter k) >>= \(w2, x) -> return (w1 `mappend` w2, x)
        Inr x -> Con (fmap handleWriter x)

handleFail :: Functor g => Free (Fail + g) a -> Free g (Maybe a)
handleFail = \y -> case y of
    Var x -> Var (Just x)
    Con op -> case op of
        Inl x -> case x of
            Fail -> Var Nothing
        Inr x -> Con (fmap handleFail x)

handleNondet :: Functor g => Free (Nondet + g) a -> Free g [a]
handleNondet = \y -> case y of
    Var x -> Var [x]
    Con op -> case op of
        Inl x -> case x of
            Or l r -> (handleNondet l >>= (\ll -> handleNondet r >>= (\rr -> Var (ll ++ rr))))
        Inr x -> Con (fmap handleNondet x)

handleVoid :: Free Void a -> a
handleVoid = \y -> case y of
    Var x -> x
    Con op -> undefined

handleWriter' :: (Functor g, Monoid w) => Free (Writer w + g) a -> Free g (w, a)
handleWriter' y = runComp (runCod (genWriter return Con (>>=))
                                  (foldFree return (algWriter return Con (>>=)) y))

genWriter :: (Functor g, Monoid w) =>
        (forall a . a -> c a) ->
      (forall a . g (c a) -> c a) ->
      (forall a b . c a -> (a -> c b) -> c b) ->
      a ->
      Comp c ((,) w) a
genWriter var con bind x = Comp (var (mempty, x))

algWriter :: (Functor g, Monoid w) =>
        (forall a . a -> c a) ->
      (forall a . g (c a) -> c a) ->
      (forall a b . c a -> (a -> c b) -> c b) ->
      (Writer w + g) (Codensity (Comp c ((,) w)) a) ->
      Codensity (Comp c ((,) w)) a
algWriter var con bind op =  Codensity (\k -> Comp (
        (\op -> case op of
            Inl x -> case x of
                Tell w1 k -> k `bind` \(w2, x) -> var (w1 `mappend` w2, x)
            Inr x -> con x)
        (fmap (\m -> runComp (unCod m k)) op)))
