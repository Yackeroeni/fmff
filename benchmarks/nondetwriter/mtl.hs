{-#LANGUAGE GADTs, RankNTypes, TypeOperators, UndecidableInstances,
 FlexibleInstances, FunctionalDependencies, FlexibleContexts, AllowAmbiguousTypes,
 DeriveFunctor #-}

import ListT
import Control.Monad.Trans.Maybe
import Control.Monad.Writer 
import Control.Monad.Identity
import Criterion.Main

main :: IO ()
--main = print $ handler (writingCoin 10)
main = defaultMain [
    bgroup "" [ bench "1" $ nf benchMe 1
                , bench "2" $ nf benchMe 2
                , bench "4" $ nf benchMe 4
                , bench "8" $ nf benchMe 8
                , bench "16" $ nf benchMe 16
                , bench "32" $ nf benchMe 32
                , bench "64" $ nf benchMe 64
                , bench "128" $ nf benchMe 128
                , bench "256" $ nf benchMe 256
                , bench "512" $ nf benchMe 512
                , bench "1024" $ nf benchMe 1024
                ]
    ]

benchMe :: Int -> [Maybe (Bool, String)]
benchMe n = handler (writingCoin n)

handler :: Monoid w => WriterT w (MaybeT (ListT Identity)) a -> [Maybe (a, w)]
handler = runIdentity . toList . runMaybeT . runWriterT

writingCoin :: Int -> WriterT String (MaybeT (ListT Identity)) Bool
writingCoin 0 = tell " Done " >> return True
writingCoin n = tell " Rec " >> WriterT (MaybeT (
                                        (return Nothing) `mappend` 
                                        (runMaybeT (runWriterT (tell (show n) >> writingCoin (n-1)))) `mappend` 
                                        (return $ Just (False, "False"))))

simple :: Int -> ListT Identity Bool
simple 0 = return True
simple n = simple (n-1) `mappend` return False

simple' :: Int -> MaybeT (ListT Identity) Bool
simple' 0 = return True
simple' n = MaybeT $ (return $ Nothing) `mappend` runMaybeT (simple' (n-1)) `mappend` (return $ Just False)
