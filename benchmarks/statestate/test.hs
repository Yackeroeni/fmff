{-# LANGUAGE GADTs, TypeOperators, RankNTypes #-}
 
import WhatMorphism.Free
import Criterion.Main



main = defaultMain [
    bgroup ""   [ bench "1" $ nf benchMe 1
                , bench "2" $ nf benchMe 2
                , bench "4" $ nf benchMe 4
                , bench "8" $ nf benchMe 8
                , bench "16" $ nf benchMe 16
                , bench "32" $ nf benchMe 32
                , bench "64" $ nf benchMe 64
                , bench "128" $ nf benchMe 128
                , bench "256" $ nf benchMe 256
                , bench "512" $ nf benchMe 512
                , bench "1024" $ nf benchMe 1024
                , bench "2048" $ nf benchMe 2048
                , bench "4096" $ nf benchMe 4096
                , bench "8192" $ nf benchMe 8192
                , bench "16384" $ nf benchMe 16384
                , bench "32768" $ nf benchMe 32768
                , bench "65536" $ nf benchMe 65536
                , bench "131072" $ nf benchMe 131072
                , bench "262144" $ nf benchMe 262144
                , bench "524288" $ nf benchMe 524288
                , bench "1048576" $ nf benchMe 1048576
                , bench "2097152" $ nf benchMe 2097152
                ]
    ]
    

benchMe :: Int -> Int
benchMe n = (handleVoid (handleState (handleState (program n) 0) 0))
--benchMe = handleDual'

program :: Int -> Free (State Int + (State Int + g)) Int
program n
    | n <= 0 = Con (Inl (Get Var))
    | otherwise = Con (Inl (Get (\s -> Con (Inl (Put (s + n) (program' (n - 1)))))))

program' :: Int -> Free (State Int + (State Int + g)) Int
program' n
    | n <= 0 = Con (Inr (Inl (Get Var)))
    | otherwise = Con (Inr (Inl (Get (\s -> Con (Inr (Inl (Put (s + n) (program (n - 1)))))))))

data Void k
instance Functor Void

handleVoid :: Free Void a -> a
handleVoid (Var x) = x
handleVoid _ = undefined

handleState :: (Functor g, Show s, Show a) => Free (State s + g) a -> s -> Free g a
handleState = \y s -> case y of
    Var x -> Var x
    Con op -> case op of
        Inl x -> case x of
            Put s' k -> handleState k s'
            Get sk   -> handleState (sk s) s
        Inr x -> Con (fmap (\m -> m s) (fmap handleState x))

data State s k where
    Put :: s -> k -> State s k
    Get :: (s -> k) -> State s k

instance Functor (State s) where
    fmap f (Put s k) = Put s (f k)
    fmap f (Get k) = Get (f . k)
