import os

tests = ["statewriter", "writerstate", "statend", "ndstate", "statestate", "nondetwriter"]
#tests = ["statend", "ndstate", "statestate", "nondetwriter"]
#tests = ["statend", "ndstate", "statestate"]
#tests = ["statend"]

for t in tests:
    os.chdir(t)
    os.system("rm *.csv")
    os.system("rm *.html")
    os.system("rm *.hi")
    os.system("rm *.o")
    os.system("stack ghc -- --make -fforce-recomp -package what-morphism -fplugin WhatMorphism test.hs")
    os.system("rm *.hi")
    os.system("rm *.o")
    os.system("mv test fused")
    os.system("stack ghc -- -fforce-recomp -O2 test.hs")
    os.system("rm *.hi")
    os.system("rm *.o")
    os.system("mv test unfused")
    os.system("stack ghc -- -fforce-recomp -O2 mtl.hs")
    os.system("rm *.hi")
    os.system("rm *.o")
    os.system("./fused --csv fused.csv --output fused.html")
    os.system("./unfused --csv unfused.csv --output unfused.html")
    os.system("./mtl --csv mtl.csv --output mtl.html")
    os.chdir("..")
