{-# LANGUAGE GADTs, TypeOperators #-}

import Control.Monad.State
import Control.Monad.Writer
import Control.Monad.Identity

import Criterion.Main

main = defaultMain [
    bgroup "" [ bench "1" $ nf benchMe 1
                , bench "2" $ nf benchMe 2
                , bench "4" $ nf benchMe 4
                , bench "8" $ nf benchMe 8
                , bench "16" $ nf benchMe 16
                , bench "32" $ nf benchMe 32
                , bench "64" $ nf benchMe 64
                , bench "128" $ nf benchMe 128
                , bench "256" $ nf benchMe 256
                , bench "512" $ nf benchMe 512
                , bench "1024" $ nf benchMe 1024
                , bench "2048" $ nf benchMe 2048
                , bench "4096" $ nf benchMe 4096
                ]
    ]

{-main = print $ benchMe 10-}

benchMe :: Int -> (Int, [Int])
benchMe n = runIdentity (evalStateT (runWriterT (program n)) 0)

program :: Int -> WriterT [Int] (StateT Int Identity) Int
program n
    | n <= 0    = get
    | otherwise = get >>= \s ->
                  put (s + n) >> 
                  tell [s] >>
                  program (n-1)

