{-# LANGUAGE GADTs, TypeOperators #-}
 
import WhatMorphism.Free
import Criterion.Main


main = defaultMain [
    bgroup "" [ bench "1" $ nf benchMe 1
                , bench "2" $ nf benchMe 2
                , bench "4" $ nf benchMe 4
                , bench "8" $ nf benchMe 8
                , bench "16" $ nf benchMe 16
                , bench "32" $ nf benchMe 32
                , bench "64" $ nf benchMe 64
                , bench "128" $ nf benchMe 128
                , bench "256" $ nf benchMe 256
                , bench "512" $ nf benchMe 512
                , bench "1024" $ nf benchMe 1024
                , bench "2048" $ nf benchMe 2048
                , bench "4096" $ nf benchMe 4096
                , bench "8192" $ nf benchMe 8192
                , bench "16384" $ nf benchMe 16384
                , bench "32768" $ nf benchMe 32768
                , bench "65536" $ nf benchMe 65536
                , bench "131072" $ nf benchMe 131072
                , bench "262144" $ nf benchMe 262144
                , bench "524288" $ nf benchMe 524288
                , bench "1048576" $ nf benchMe 1048576
                ]
    ]


{-main = print $ benchMe 10-}
    

benchMe :: Int -> ([Int], Int)
benchMe n = (handleVoid (handleWriter (handleState (program n) 0)))

program :: Int -> Free (State Int + (Writer [Int] + g)) Int
program n
    | n <= 0 = Con (Inl (Get Var))
    | otherwise = Con (Inl (Get (\s -> Con (Inl (Put (s + n) 
                    (Con (Inr (Inl (Tell [s] (program (n - 1)))))))))))

data Void k
instance Functor Void

handleVoid :: Free Void a -> a
handleVoid (Var x) = x
handleVoid _ = undefined

handleState :: (Functor g, Show s, Show a) => Free (State s + g) a -> s -> Free g a
handleState = \y s -> case y of
    Var x -> Var x
    Con op -> case op of
        Inl x -> case x of
            Put s' k -> handleState k s'
            Get sk   -> handleState (sk s) s
        Inr x -> Con (fmap (\m -> m s) (fmap handleState x))

handleWriter :: (Monoid w, Functor g) => Free (Writer w + g) a -> Free g (w, a)
handleWriter = \y -> case y of
    Var x -> return (mempty, x)
    Con op -> case op of
        Inl x -> case x of
            Tell w1 k -> (handleWriter k) >>= \(w2, x) -> return (w1 `mappend` w2, x)
        Inr x -> Con (fmap handleWriter x)

data Writer w k where
    Tell :: w -> k -> Writer w k

instance Functor (Writer w) where
    fmap f (Tell w k) = Tell w (f k)

data State s k where
    Put :: s -> k -> State s k
    Get :: (s -> k) -> State s k

instance Functor (State s) where
    fmap f (Put s k) = Put s (f k)
    fmap f (Get k) = Get (f . k)

data Nondet k where
    Or :: k -> k -> Nondet k

instance Functor Nondet where
    fmap f (Or l r) = Or (f l) (f r)

handleNondet :: Functor g => Free (Nondet + g) a -> Free g [a]
handleNondet = \y -> case y of
    Var x -> Var [x]
    Con op -> case op of
        Inl x -> case x of
            Or l r -> (handleNondet l >>= (\ll -> handleNondet r >>= (\rr -> Var (ll ++ rr))))
        Inr x -> Con (fmap handleNondet x)
