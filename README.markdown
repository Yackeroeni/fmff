Fast Monads for Free
====================

An extension of WhatMorphism (<https://github.com/jaspervdj/what-morphism>).

This plugin automatically fuses algebraic effect handlers for free monads.

Configuring & installing
------------------------

The plugin is available as a stack build. Simply install stack (cabal install
stack), clone this repository and run "stack install".


Fusing handlers
---------------

In order to transform handlers into fused form, simply run ghc (stack ghc) with
the plugin enabled (i.e. stack ghc -- -package what-morphism -fplugin WhatMorphism).

There are a few prerequisites to allow fusion:


- the definitions of Free, Var, Con from Fmff.Free have to be used
- handlers need to be used in the same module where they are defined
- handlers need to be in case-split form without using the `junction' operator
    - a fold-form is also allowed for handlers without parameters, for
handlers with parameters (such as State), the program results may be unexpected
- handlers with parameters can only be fused on the right-hand side (i.e.
  (Nondet + State s) can be fused, (State s + Nondet) cannot.


Example
-------

A nice example to get you started is the statend test:


~~~~
import WhatMorphism.Free
import Criterion.Main


main :: IO ()
main = print $ (handleVoid (handleState (handleNondet (program 10)) 0))
    
program :: Int -> Free (Nondet + (State Int + g)) Int
program n
    | n <= 0 = Con (Inr (Inl (Get Var)))
    | otherwise = Con (Inr (Inl (Get (\s -> Con (Inr (Inl (Put (s + n) 
                    (Con (Inl (Or (program (n - 1)) (Var s)))))))))))

data Void k
instance Functor Void

handleVoid :: Free Void a -> a
handleVoid (Var x) = x
handleVoid _ = undefined

handleState :: (Functor g, Show s, Show a) => Free (State s + g) a -> s -> Free g a
handleState = \y s -> case y of
    Var x -> Var x
    Con op -> case op of
        Inl x -> case x of
            Put s' k -> handleState k s'
            Get sk   -> handleState (sk s) s
        Inr x -> Con (fmap (\m -> m s) (fmap handleState x))

data State s k where
    Put :: s -> k -> State s k
    Get :: (s -> k) -> State s k

instance Functor (State s) where
    fmap f (Put s k) = Put s (f k)
    fmap f (Get k) = Get (f . k)

data Nondet k where
    Or :: k -> k -> Nondet k

instance Functor Nondet where
    fmap f (Or l r) = Or (f l) (f r)

handleNondet :: Functor g => Free (Nondet + g) a -> Free g [a]
handleNondet = \y -> case y of
    Var x -> Var [x]
    Con op -> case op of
        Inl x -> case x of
            Or l r -> (handleNondet l >>= (\ll -> handleNondet r >>= (\rr -> Var (ll ++ rr))))
        Inr x -> Con (fmap handleNondet x)
~~~~
