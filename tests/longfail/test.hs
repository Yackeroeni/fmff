{-#LANGUAGE GADTs, RankNTypes, TypeOperators, UndecidableInstances,
 FlexibleInstances, FunctionalDependencies, FlexibleContexts, AllowAmbiguousTypes,
 DeriveFunctor #-}

import Data.Monoid
import WhatMorphism.Free

main = print $ handleWriter (long 0)

--long :: Functor g => Int -> Free (Writer String + g) Bool
long 0 = Var True
long n = Con (Inl (Tell "5" (long (n - 1))))

data Void k

instance Functor Void 

data Writer w k where
    Tell :: w -> k -> Writer w k

instance Functor (Writer w) where
    fmap f (Tell w k) = Tell w (f k)

handleWriter :: (Monoid w, Functor g) => Free (Writer w + g) a -> Free g (w, a)
handleWriter = \y -> case y of
    Var x -> return (mempty, x)
    Con op -> case op of
        Inl x -> case x of
            Tell w1 k -> (handleWriter k) >>= \(w2, x) -> return (w1 `mappend` w2, x)
        Inr x -> Con (fmap handleWriter x)

handleWriter' :: (Functor g, Monoid w) => Free (Writer w + g) a -> Free g (w, a)
handleWriter' y = runComp (runCod (genWriter return Con (>>=))
                                  (foldFree return (algWriter return Con (>>=)) y))

genWriter :: (Functor g, Monoid w) =>
        (forall a . a -> c a) ->
      (forall a . g (c a) -> c a) ->
      (forall a b . c a -> (a -> c b) -> c b) ->
      a ->
      Comp c ((,) w) a
genWriter var con bind x = Comp (var (mempty, x))

algWriter :: (Functor g, Monoid w) =>
        (forall a . a -> c a) ->
      (forall a . g (c a) -> c a) ->
      (forall a b . c a -> (a -> c b) -> c b) ->
      (Writer w + g) (Codensity (Comp c ((,) w)) a) ->
      Codensity (Comp c ((,) w)) a
algWriter var con bind op =  Codensity (\k -> Comp (
        (\op -> case op of
            Inl x -> case x of
                Tell w1 k -> k `bind` \(w2, x) -> var (w1 `mappend` w2, x)
            Inr x -> con x)
        (fmap (\m -> runComp (unCod m k)) op)))
