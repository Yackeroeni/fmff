{-#LANGUAGE GADTs, RankNTypes, TypeOperators, UndecidableInstances,
 FlexibleInstances, FunctionalDependencies, FlexibleContexts, AllowAmbiguousTypes,
 DeriveFunctor #-}

import WhatMorphism.Free

main = print $ (handleVoid . handleNondet . handleFail) drunkCoin

data Nondet k where
    Or :: k -> k -> Nondet k

instance Functor Nondet where
    fmap f (Or l r) = Or (f l) (f r)

instance (Show k) => Show (Nondet k) where
    show (Or l r) = "Or (" ++ show l ++ ") ("  ++ show r ++ ")"

data Fail k where
    Fail :: Fail k

instance Functor Fail where
    fmap _ Fail = Fail

instance Show (Fail k) where
    show Fail = "Fail"

data Void k

instance Show (Void k) where

instance Functor Void 

drunkCoin :: Free (Fail + (Nondet + g)) Bool
drunkCoin = Con (Inr (Inl (Or
            (Con (Inl Fail))
            (Con (Inr (Inl (Or
                (Var True)
                (Var False)
            ))))
        )))

handleFail :: Functor g => Free (Fail + g) a -> Free g (Maybe a)
handleFail = \y -> case y of
    Var x -> Var (Just x)
    Con op -> case op of
        Inl x -> case x of
            Fail -> Var Nothing
        Inr x -> Con (fmap handleFail x)

handleNondet :: Functor g => Free (Nondet + g) a -> Free g [a]
handleNondet = \y -> case y of
    Var x -> Var [x]
    Con op -> case op of
        Inl x -> case x of
            Or l r -> (handleNondet l >>= (\ll -> handleNondet r >>= (\rr -> Var (ll ++ rr))))
        Inr x -> Con (fmap handleNondet x)

handleVoid :: Free Void a -> a
handleVoid = \y -> case y of
    Var x -> x
    Con op -> undefined

{-handleCoin :: Free Void (Maybe [Bool])-}
{-handleCoin = (runComp (runCod (genFail return Con (>>=)) -}
                {-(runComp (runCod (genNondet return (algFail return Con (>>=)) (>>=)) -}
                    {-(foldFree return (algNondet return (algFail return Con (>>=)) (>>=)) (>>=) drunkCoin)))))-}

genFail :: Functor g => (forall a . a -> c a) ->
                     (forall a . g (c a) -> c a) ->
                     (forall a b . c a -> (a -> c b) -> c b) ->
                     a -> 
                     (Comp c Maybe) a
genFail var con bind x = Comp (var (Just x)) 

algFail :: Functor g => (forall a . a -> c a) ->
                     (forall a . g (c a) -> c a) ->
                     (forall a b . c a -> (a -> c b) -> c b) ->
                     (Fail + g) (Codensity (Comp c Maybe) a) -> 
                     Codensity (Comp c Maybe) a
algFail var con bind = \op -> Codensity (\k -> Comp (
            (\op -> case op of
                Inl x -> case x of
                    Fail -> var Nothing
                Inr x -> con x)
            (fmap (\m -> runComp (unCod m k)) op)))


genNondet :: Functor g => (forall a . a -> c a) ->
                     (forall a . g (c a) -> c a) ->
                     (forall a b . c a -> (a -> c b) -> c b) ->
                     a -> 
                     Comp c [] a
genNondet var con bind x = Comp (var [x]) 

algNondet :: Functor g => (forall a . a -> c a) ->
                     (forall a . g (c a) -> c a) ->
                     (forall a b . c a -> (a -> c b) -> c b) ->
                     (Nondet + g) (Codensity (Comp c []) a) -> 
                     Codensity (Comp c []) a
algNondet var con bind = \op -> Codensity (\k -> Comp (
            (\op -> case op of
                Inl x -> case x of
                    Or l r -> (bind l (\ll -> bind r (\rr -> var (ll ++ rr))))
                Inr x -> con x)
            (fmap (\m -> runComp (unCod m k)) op)))
