{-# LANGUAGE GADTs, TypeOperators, RankNTypes  #-}
 
import WhatMorphism.Free

main = print $ benchMe 10

benchMe :: Int -> [Int]
benchMe n = (handleVoid (handleNondet (handleState (program n) 0)))

program :: Int -> Free (State Int + (Nondet + g)) Int
program n
    | n <= 0 = Con (Inl (Get Var))
    | otherwise = Con (Inl (Get (\s -> Con (Inl (Put (s + n)
                    (Con (Inr (Inl (Or (program (n - 1)) (Var s))))))))))

data Void k
instance Functor Void

handleVoid :: Free Void a -> a
handleVoid (Var x) = x
handleVoid _ = undefined

handleState :: (Functor g, Show s, Show a) => Free (State s + g) a -> s -> Free g a
handleState = \y s -> case y of
    Var x -> Var x
    Con op -> case op of
        Inl x -> case x of
            Put s' k -> handleState k s'
            Get sk   -> handleState (sk s) s
        Inr x -> Con (fmap (\m -> m s) (fmap handleState x))

data State s k where
    Put :: s -> k -> State s k
    Get :: (s -> k) -> State s k

instance Functor (State s) where
    fmap f (Put s k) = Put s (f k)
    fmap f (Get k) = Get (f . k)

data Nondet k where
    Or :: k -> k -> Nondet k

instance Functor Nondet where
    fmap f (Or l r) = Or (f l) (f r)

handleNondet :: Functor g => Free (Nondet + g) a -> Free g [a]
handleNondet = \y -> case y of
    Var x -> Var [x]
    Con op -> case op of
        Inl x -> case x of
            Or l r -> (handleNondet l >>= (\ll -> handleNondet r >>= (\rr -> Var (ll ++ rr))))
        Inr x -> Con (fmap handleNondet x)
