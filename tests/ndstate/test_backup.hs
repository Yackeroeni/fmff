{-# LANGUAGE GADTs, TypeOperators, RankNTypes  #-}
 
import WhatMorphism.Free
import Criterion.Main


main = print $ benchMe 10
--main = print $ hndp 10
    

benchMe :: Int -> [Int]
benchMe n = (handleVoid (handleNondet (handleState (program n) 0)))

program :: Int -> Free (State Int + (Nondet + g)) Int
program n
    | n <= 0 = Con (Inl (Get Var))
    | otherwise = Con (Inl (Get (\s -> Con (Inl (Put (s + n)
                    (Con (Inr (Inl (Or (program (n - 1)) (Var s))))))))))

data Void k
instance Functor Void

handleVoid :: Free Void a -> a
handleVoid (Var x) = x
handleVoid _ = undefined

handleState :: (Functor g, Show s, Show a) => Free (State s + g) a -> s -> Free g a
handleState = \y s -> case y of
    Var x -> Var x
    Con op -> case op of
        Inl x -> case x of
            Put s' k -> handleState k s'
            Get sk   -> handleState (sk s) s
        Inr x -> Con (fmap (\m -> m s) (fmap handleState x))

data State s k where
    Put :: s -> k -> State s k
    Get :: (s -> k) -> State s k

instance Functor (State s) where
    fmap f (Put s k) = Put s (f k)
    fmap f (Get k) = Get (f . k)

data Nondet k where
    Or :: k -> k -> Nondet k

instance Functor Nondet where
    fmap f (Or l r) = Or (f l) (f r)

handleNondet :: Functor g => Free (Nondet + g) a -> Free g [a]
handleNondet = \y -> case y of
    Var x -> Var [x]
    Con op -> case op of
        Inl x -> case x of
            Or l r -> (handleNondet l >>= (\ll -> handleNondet r >>= (\rr -> Var (ll ++ rr))))
        Inr x -> Con (fmap handleNondet x)

hndp :: Int -> [Int]
hndp n =
    handleVoid $
        runComp (
            runCod 
                (genNondet return Con (>>=))
                (runComp (
                    runCod
                        (genState return (algNondet return Con (>>=)) (>>=))
                        (foldFree
                            return
                            (algState return (algNondet return Con (>>=)) (>>=)) 
                            (program n)))
                    0))

genState :: Functor g =>
          (forall a . a -> c a) ->
          (forall a . g (c a) -> c a) ->
          (forall a b . c a -> (a -> c b) -> c b) ->
          a -> 
          Comp ((->) s) c a
genState var con bind x = Comp (\s -> var x)

algState :: Functor g =>
          (forall a . a -> c a) ->
          (forall a . g (c a) -> c a) ->
          (forall a b . c a -> (a -> c b) -> c b) ->
          (State s + g) (Codensity (Comp ((->) s) c) a) -> 
          Codensity (Comp ((->) s) c) a
algState var con bind op = Codensity (\k -> Comp (
    (\op s -> case op of
        Inl x -> case x of
            Put s' k -> k s'
            Get k -> (k s) s
        Inr x -> con (fmap (\m -> m s) x))
        (fmap (\m -> runComp (runCod k m)) op)))


genNondet :: Functor g => (forall a . a -> c a) ->
                     (forall a . g (c a) -> c a) ->
                     (forall a b . c a -> (a -> c b) -> c b) ->
                     a -> 
                     Comp c [] a
genNondet var con bind x = Comp (var [x]) 

algNondet :: Functor g => (forall a . a -> c a) ->
                     (forall a . g (c a) -> c a) ->
                     (forall a b . c a -> (a -> c b) -> c b) ->
                     (Nondet + g) (Codensity (Comp c []) a) -> 
                     Codensity (Comp c []) a
algNondet var con bind = \op -> Codensity (\k -> Comp (
            (\op -> case op of
                Inl x -> case x of
                    Or l r -> (bind l (\ll -> bind r (\rr -> var (ll ++ rr))))
                Inr x -> con x)
            (fmap (\m -> runComp (runCod k m)) op)))
