{-# LANGUAGE GADTs, TypeOperators, RankNTypes #-}
 
import WhatMorphism.Free
import Criterion.Main



main = print $ benchMe 10
    
benchMe :: Int -> Int
benchMe n = (handleVoid (handleState'' (handleState'' (program n) 0) 0))
--benchMe n = handleDual' n

program :: Int -> Free (State Int + (State Int + g)) Int
program n
    | n <= 0 = Con (Inl (Get Var))
    | otherwise = Con (Inl (Get (\s -> Con (Inl (Put (s + n) (program' (n - 1)))))))

program' :: Int -> Free (State Int + (State Int + g)) Int
program' n
    | n <= 0 = Con (Inr (Inl (Get Var)))
    | otherwise = Con (Inr (Inl (Get (\s -> Con (Inr (Inl (Put (s + n) (program (n - 1)))))))))

data Void k
instance Functor Void

handleDual' :: Int -> Int
handleDual' n =
    handleVoid
        ((runComp :: Comp ((->) Int) (Free Void) Int -> Int -> Free Void Int)
            ((runCod :: (Int -> Comp ((->) Int) (Free Void) Int) -> Codensity (Comp ((->) Int) (Free Void)) Int -> Comp ((->) Int) (Free Void) Int) 
                ((\var con bind x -> Comp (\s -> var x))
                    (return :: forall a. a -> Free Void a) 
                    Con 
                    ((>>=) :: forall a b. Free Void a -> (a -> Free Void b) -> Free Void b))
                ((runComp :: Comp ((->) Int) (Codensity (Comp ((->) Int) (Free Void))) Int -> Int -> Codensity (Comp ((->) Int) (Free Void)) Int)
                    ((runCod :: (Int -> Comp ((->) Int) (Codensity (Comp ((->) Int) (Free Void))) Int) -> Codensity (Comp ((->) Int) (Codensity (Comp ((->) Int) (Free Void)))) Int -> Comp ((->) Int) (Codensity (Comp ((->) Int) (Free Void))) Int)
                        ((\var con bind x -> Comp (\s -> var x)) 
                            (return :: forall a. a -> Codensity (Comp ((->) Int) (Free Void)) a)
                            (algState 
                                (return :: forall a. a -> Free Void a) 
                                Con 
                                ((>>=) :: forall a b. Free Void a -> (a -> Free Void b) -> Free Void b))
                            ((>>=) :: forall a b. Codensity (Comp ((->) Int) (Free Void)) a -> (a -> Codensity (Comp ((->) Int) (Free Void)) b) -> Codensity (Comp ((->) Int) (Free Void)) b))
                        (foldFree
                            (return :: forall a. a -> Codensity (Comp ((->) Int) (Codensity (Comp ((->) Int) (Free Void)))) a)
                            (algState 
                                (return :: forall a. a -> Codensity (Comp ((->) Int) (Free Void)) a) 
                                (algState 
                                    (return :: forall a. a -> Free Void a) 
                                    Con 
                                    ((>>=) :: forall a b. Free Void a -> (a -> Free Void b) -> Free Void b))
                                ((>>=) :: forall a b. Codensity (Comp ((->) Int) (Free Void)) a -> (a -> Codensity (Comp ((->) Int) (Free Void)) b) -> Codensity (Comp ((->) Int) (Free Void)) b))
                            (program n)))
                0))
        0)

handleVoid :: Free Void a -> a
handleVoid (Var x) = x
handleVoid _ = undefined

handleState :: (Functor g, Show s, Show a) => Free (State s + g) a -> s -> Free g a
handleState = \y s -> case y of
    Var x -> Var x
    Con op -> case op of
        Inl x -> case x of
            Put s' k -> handleState k s'
            Get sk   -> handleState (sk s) s
        Inr x -> Con (fmap (\m -> m s) (fmap handleState x))



data State s k where
    Put :: s -> k -> State s k
    Get :: (s -> k) -> State s k

instance Functor (State s) where
    fmap f (Put s k) = Put s (f k)
    fmap f (Get k) = Get (f . k)

genState :: Functor g =>
          (forall a . a -> c a) ->
          (forall a . g (c a) -> c a) ->
          (forall a b . c a -> (a -> c b) -> c b) ->
          a -> 
          Comp ((->) s) c a
genState var con bind x = Comp (\s -> var x)

algState :: Functor g =>
          (forall a . a -> c a) ->
          (forall a . g (c a) -> c a) ->
          (forall a b . c a -> (a -> c b) -> c b) ->
          (State s + g) (Codensity (Comp ((->) s) c) a) -> 
          Codensity (Comp ((->) s) c) a
algState var con bind op = Codensity (\k -> Comp (
    (\op s -> case op of
        Inl x -> case x of
            Put s' k -> k s'
            Get k -> (k s) s
        Inr x -> con (fmap (\m -> m s) x))
        (fmap (\m -> runComp (runCod k m)) op)))

{-# NOINLINE handleState' #-}
handleState' :: Functor g => Free (State s + g) a -> s -> Free g a
handleState' y = foldFree genState' algState' y

genState' ::  a -> s -> Free g a
genState' x s = Var x

algState' :: Functor g => (State s + g) (s -> Free g a) -> s -> Free g a
algState' op s = case op of
                Inl x -> case x of
                    Put s' k -> k s'
                    Get k -> k s s
                Inr x -> Con (fmap (\m -> m s) x)

{-# NOINLINE handleState'' #-}
handleState'' :: Functor g => Free (State s + g) a -> s -> Free g a
handleState'' y = foldFree 
        (\x s -> Var x) 
        (\op s -> case op of
            Inl x -> case x of
                Put s' k -> k s'
                Get k -> k s s
            Inr x -> Con (fmap (\m -> m s) x))
        y
