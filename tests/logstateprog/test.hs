{-#LANGUAGE GADTs, RankNTypes, TypeOperators, UndecidableInstances,
 FlexibleInstances, FunctionalDependencies, FlexibleContexts, AllowAmbiguousTypes,
 DeriveFunctor #-}

import WhatMorphism.Free

main = print $ (handleVoid (handleWriter (handleLogState (program 5) 4)))

data Void k

instance Functor Void 

handleVoid :: Free Void a -> a
handleVoid = \y -> case y of
    Var x -> x
    Con op -> undefined

program :: Int -> Free (State Int + g) Int
program n
    | n <= 0 = Con (Inl (Get Var))
    | otherwise = Con (Inl (Get (\s -> Con (Inl (Put (s + n) (program (n - 1)))))))

handleLogState :: Functor g => Free (State s + g) a -> s -> Free (Writer String + g) a
handleLogState = \y s -> case y of
    Var x -> Var x
    Con op -> case op of
        Inl x -> case x of
            Put s' k -> Con (Inl (Tell "put" (handleLogState k s')))
            Get k -> handleLogState (k s) s
        Inr x -> Con (Inr (fmap (\m -> m s) (fmap handleLogState x)))

--------------------------------------------------------------------------------

handleWriter :: (Functor g, Monoid w) => Free (Writer w + g) a -> Free g (w, a)
handleWriter = \y -> case y of
    Var x -> return (mempty, x)
    Con op -> case op of
        Inl x -> case x of
            Tell w1 k -> (handleWriter k) >>= \(w2, x) -> return (w1 `mappend` w2, x)
        Inr x -> Con (fmap handleWriter x)

--------------------------------------------------------------------------------

handleWriter' :: (Functor g, Monoid w) => Free (Writer w + g) a -> Free g (w, a)
handleWriter' y = runComp (runCod (genWriter return Con (>>=))
                                  (foldFree return (algWriter return Con (>>=)) y))

genWriter :: (Functor g, Monoid w) =>
        (forall a . a -> c a) ->
      (forall a . g (c a) -> c a) ->
      (forall a b . c a -> (a -> c b) -> c b) ->
      a ->
      Comp c ((,) w) a
genWriter var con bind x = Comp (var (mempty, x))

algWriter :: (Functor g, Monoid w) =>
        (forall a . a -> c a) ->
      (forall a . g (c a) -> c a) ->
      (forall a b . c a -> (a -> c b) -> c b) ->
      (Writer w + g) (Codensity (Comp c ((,) w)) a) ->
      Codensity (Comp c ((,) w)) a
algWriter var con bind op =  Codensity (\k -> Comp (
        (\op -> case op of
            Inl x -> case x of
                Tell w1 k -> k `bind` \(w2, x) -> var (w1 `mappend` w2, x)
            Inr x -> con x)
        (fmap (\m -> runComp (runCod k m)) op)))

handleLogState' :: Functor g => Free (State s + g) a -> s -> Free (Writer String + g) a
handleLogState' y = runComp (runCod (genLogState return Con (>>=))
                                    (foldFree return (algLogState return Con (>>=)) y))

genLogState :: Functor g =>
          (forall a . a -> c a) ->
          (forall a . g (c a) -> c a) ->
          (forall a b . c a -> (a -> c b) -> c b) ->
          a -> 
          Comp ((->) s) c a
genLogState var con bind x = Comp (\s -> var x)

algLogState :: Functor g =>
          (forall a . a -> c a) ->
          (forall a . (Writer String + g) (c a) -> c a) ->
          (forall a b . c a -> (a -> c b) -> c b) ->
          (State s + g) (Codensity (Comp ((->) s) c) a) -> 
          Codensity (Comp ((->) s) c) a
algLogState var con bind op = Codensity (\k -> Comp (
        (\op s -> case op of
            Inl x -> case x of
                Put s' k -> con (Inl (Tell "put" (k s')))
                Get k -> (k s) s
            Inr x -> con (Inr (fmap (\m -> m s) x)))
        (fmap (\m -> runComp (runCod k m)) op)))

-- State

data State s k where
    Put :: s -> k -> State s k
    Get :: (s -> k) -> State s k

instance Functor (State s) where
    fmap f (Put s k) = Put s (f k)
    fmap f (Get k) = Get (f . k)

-- Writer

data Writer w k where
    Tell :: w -> k -> Writer w k

instance Functor (Writer w) where
    fmap f (Tell w k) = Tell w (f k)
