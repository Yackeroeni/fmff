{-#LANGUAGE GADTs, RankNTypes, TypeOperators, UndecidableInstances,
 FlexibleInstances, FunctionalDependencies, FlexibleContexts, AllowAmbiguousTypes,
 DeriveFunctor #-}

import WhatMorphism.Free

main = print $ (handleVoid (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet (handleNondet  coin))))))))))))))))))))))))))))))))))))))))))))))

data Nondet k where
    Or :: k -> k -> Nondet k

instance Functor Nondet where
    fmap f (Or l r) = Or (f l) (f r)

instance (Show k) => Show (Nondet k) where
    show (Or l r) = "Or (" ++ show l ++ ") ("  ++ show r ++ ")"

data Void k

instance Functor Void 

--coin :: Free (Nondet + (Nondet + (Nondet + (Nondet + (Nondet + (Nondet + g)))))) Int
coin =  (Con (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inl (Or
                        (Con (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inl (Or 
                            (Con (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inl (Or 
                                (Var 1)
                                (Var 10)
                            )))))))))))))))))))))))))))))))))))))))))))))))
                            (Con (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inl (Or 
                                (Var 2)
                                (Var 30)
                            )))))))))))))))))))))))))))))))))))))))))))))))
                        ))))))))))))))))))))))))))))))))))))))))))))))
                        (Con (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inl (Or 
                            (Con (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inl (Or 
                                (Var 100)
                                (Var 1000)
                            )))))))))))))))))))))))))))))))))))))))))))))))
                            (Con (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inr (Inl (Or 
                                (Var 234)
                                (Var 3020934)
                            )))))))))))))))))))))))))))))))))))))))))))))))
                        ))))))))))))))))))))))))))))))))))))))))))))))
                    )))))))))))))))))))))))))))))))))))))))))))))

handleNondet :: Functor g => Free (Nondet + g) a -> Free g [a]
handleNondet = \y -> case y of
    Var x -> Var [x]
    Con op -> case op of
        Inl x -> case x of
            Or l r -> (handleNondet l >>= (\ll -> handleNondet r >>= (\rr -> Var (ll ++ rr))))
        Inr x -> Con (fmap handleNondet x)

handleVoid :: Free Void a -> a
handleVoid = \y -> case y of
    Var x -> x
    Con op -> undefined
