{-#LANGUAGE GADTs, RankNTypes, TypeOperators, UndecidableInstances,
 FlexibleInstances, FunctionalDependencies, FlexibleContexts, AllowAmbiguousTypes,
 DeriveFunctor #-}

import WhatMorphism.Free

main = print $ (handleVoid . handleNondet) coin

data Nondet k where
    Or :: k -> k -> Nondet k

instance Functor Nondet where
    fmap f (Or l r) = Or (f l) (f r)

instance (Show k) => Show (Nondet k) where
    show (Or l r) = "Or (" ++ show l ++ ") ("  ++ show r ++ ")"

data Void k

instance Functor Void 

{-# NOINLINE coin #-}
coin :: Free (Nondet + g) Bool
coin = Con (Inl (Or (Var True) (Var False)))

handleNondet :: Functor g => Free (Nondet + g) a -> Free g [a]
handleNondet = \y -> case y of
    Var x -> Var [x]
    Con op -> case op of
        Inl x -> case x of
            Or l r -> (handleNondet l >>= (\ll -> handleNondet r >>= (\rr -> Var (ll ++ rr))))
        Inr x -> Con (fmap handleNondet x)

handleVoid :: Free Void a -> a
handleVoid = \y -> case y of
    Var x -> x
    Con op -> undefined

handleNondet' :: Functor g => Free (Nondet + g) a -> Free g [a]
handleNondet' y = runComp (runCod (genNondet return Con (>>=))
                                (foldFree return (algNondet return Con (>>=)) y))

genNondet :: Functor g => (forall a . a -> c a) ->
                     (forall a . g (c a) -> c a) ->
                     (forall a b . c a -> (a -> c b) -> c b) ->
                     a -> 
                     Comp c [] a
genNondet var con bind x = Comp (var [x]) 

algNondet :: Functor g => (forall a . a -> c a) ->
                     (forall a . g (c a) -> c a) ->
                     (forall a b . c a -> (a -> c b) -> c b) ->
                     (Nondet + g) (Codensity (Comp c []) a) -> 
                     Codensity (Comp c []) a
algNondet var con bind = \op -> Codensity (\k -> Comp (
            (\op -> case op of
                Inl x -> case x of
                    Or l r -> (bind l (\ll -> bind r (\rr -> var (ll ++ rr))))
                Inr x -> con x)
            (fmap (\m -> runComp (runCod k m)) op)))
