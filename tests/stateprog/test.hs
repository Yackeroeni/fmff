{-#LANGUAGE GADTs, RankNTypes, TypeOperators, UndecidableInstances,
 FlexibleInstances, FunctionalDependencies, FlexibleContexts, AllowAmbiguousTypes,
 DeriveFunctor #-}

import WhatMorphism.Free
import Debug.Trace

main = print $ (handleVoid (hsFold (program 10) 0))

data Void k

instance Functor Void 

handleVoid :: Free Void a -> a
handleVoid = \y -> case y of
    Var x -> x
    Con op -> undefined

program :: Int -> Free (State Int + g) Int
program n
    | n <= 0 = Con (Inl (Get Var))
    | otherwise = Con (Inl (Get (\s -> Con (Inl (Put (s + n) (program (n - 1)))))))

handleState :: (Functor g, Show s, Show a) => Free (State s + g) a -> s -> Free g a
handleState = \y s -> case y of
    Var x -> Var x
    Con op -> case op of
        Inl x -> case x of
            Put s' k -> handleState k s'
            Get sk   -> handleState (sk s) s
        Inr x -> Con (fmap (\m -> m s) (fmap handleState x))

{-# NOINLINE hsFold #-}
hsFold :: Functor g => Free (State s + g) a -> s -> Free g a
hsFold = 
    foldFree 
        (\x s -> Var x) 
        (\op s -> case op of
            Inl x -> case x of
                Put s' k -> k s'
                Get k -> k s s
            Inr x -> Con (fmap (\m -> m s) x))

data State s k where
    Put :: s -> k -> State s k
    Get :: (s -> k) -> State s k

instance Functor (State s) where
    fmap f (Put s k) = Put s (f k)
    fmap f (Get k) = Get (f . k)


handleState' :: Functor g => Free (State s + g) a -> s -> Free g a
handleState' y = runComp (runCod (genState return Con (>>=))
                                    (foldFree return (algState return Con (>>=)) y))

genState :: Functor g =>
          (forall a . a -> c a) ->
          (forall a . g (c a) -> c a) ->
          (forall a b . c a -> (a -> c b) -> c b) ->
          a -> 
          Comp ((->) s) c a
genState var con bind x = Comp (\s -> var x)

algState :: Functor g =>
          (forall a . a -> c a) ->
          (forall a . g (c a) -> c a) ->
          (forall a b . c a -> (a -> c b) -> c b) ->
          (State s + g) (Codensity (Comp ((->) s) c) a) -> 
          Codensity (Comp ((->) s) c) a
algState var con bind op = Codensity (\k -> Comp (
    (\op s -> case op of
        Inl x -> case x of
            Put s' k -> k s'
            Get k -> (k s) s
        Inr x -> con (fmap (\m -> m s) x))
        (fmap (\m -> runComp (runCod k m)) op)))
